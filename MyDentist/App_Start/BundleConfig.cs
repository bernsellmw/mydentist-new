﻿using System.Web;
using System.Web.Optimization;

namespace MyDentist
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/Content").Include(
                        "~/Content/jquery.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Content/jquery.sticky.js",
                        "~/Content/jquery.stellar.min.js",
                        "~/Content/wow.min.js",
                        "~/Content/smoothscroll.js",
                        "~/Content/owl.carousel.min.js",
                        "~/Content/custom.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Content/modernizr-2.8.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/Scripts").Include(
                        "~/Scripts/animsition.min.js",
                        "~/Scripts/popper.js",
                        "~/Scripts/select2.min.js",
                        "~/Scripts/moment.min.js",
                        "~/Scripts/daterangepicker.js",
                        "~/Scripts/countdowntime.js",
                        "~/Scripts/main.js",
                        "~/Scripts/jquery-3.4.1.js"));

            bundles.Add(new ScriptBundle("~/faq/Content").Include(
                        "~/Content/jquery.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Content/jquery.sticky.js",
                        "~/Content/jquery.stellar.min.js",
                        "~/Content/wow.min.js",
                        "~/Content/smoothscroll.js",
                        "~/Content/owl.carousel.min.js",
                        "~/Content/custom.js",
                        "~/Content/faq/main.js",
                        "~/Content/faq/util.js",
                        "~/Content/modernizr-2.8.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/cssing/css").Include(
                      "~/css/animate.css",
                      "~/css/font-awesome.min.css",
                      "~/css/bootstrap.min.css",
                      "~/css/owl.carousel.css",
                      "~/css/owl.theme.default.min.css",
                      "~/css/tooplate-style.css",
                      "~/css/hamburgers.min.css",
                      "~/css/animsition.min.css",
                      "~/css/select2.min.css",
                      "~/css/daterangepicker.css",
                      "~/css/magnific-popup.css",
                      "~/css/util.css",
                      "~/css/Site.css",
                      "~/css/main.css"));

            bundles.Add(new StyleBundle("~/cssFaq/css").Include(
                      "~/css/animate.css",
                      "~/css/font-awesome.min.css",
                      "~/css/bootstrap.min.css",
                      "~/css/owl.carousel.css",
                      "~/css/owl.theme.default.min.css",
                      "~/css/tooplate-style.css",
                      "~/css/hamburgers.min.css",
                      "~/css/animsition.min.css",
                      "~/css/select2.min.css",
                      "~/css/style.css",
                      "~/css/daterangepicker.css",
                      "~/css/magnific-popup.css"));


        }
    }
}
