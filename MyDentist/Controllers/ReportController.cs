﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace MyDentist.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;

            
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/ReportMyDentistPatientUser";
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}