﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MyDentist.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Device.Location;
using MyDentist.Repositories;
using PagedList;

namespace MyDentist.Controllers
{
    public class UserDentistController : Controller
    {
        private const int pageSize = 5;
        // GET: UserDentist
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "IsEmailVerified,ActivationCode")] tbl_Dentist UserDentist)
        {
            try
            {
                bool status = false;
                string message = "";
                if (ModelState.IsValid)
                {
                    #region
                    var isExist = IsEmailExist(UserDentist.Email);
                    if (isExist)
                    {
                        ModelState.AddModelError("Email exist", "Email already Exist");
                        return View(UserDentist);
                    }
                    #endregion

                    #region Generate Activation Code
                    UserDentist.ActivationCode = Guid.NewGuid();
                    #endregion


                    #region Password Hashing
                    UserDentist.Password = Crypto.Hash(UserDentist.Password);
                    UserDentist.ConfirmPassword = Crypto.Hash(UserDentist.ConfirmPassword);
                    #endregion
                    UserDentist.IsEmailVerified = false;

                    #region Save to Database
                    using (db_mydentistEntities1 db = new db_mydentistEntities1())
                    {
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Dentist",
                            column_name = "ALL COLUMN",
                            action_date = DateTime.Today,
                            new_value = UserDentist.Name + "," + UserDentist.NPA + "," + UserDentist.Phone.ToString()
                            + "," + UserDentist.Address + "," + UserDentist.Email + "," + UserDentist.Username,

                            action_type = "CREATE"

                        };
                        db.tbl_Audit_Trail.Add(at);
                        db.tbl_Dentist.Add(UserDentist);

                        db.SaveChanges();

                        //Send Email to user
                        SendVerificationLinkEmail(UserDentist.Email, UserDentist.ActivationCode.ToString());
                        message = "Registration successfully done. Account activation link " +
                    " has been sent to your email id:" + UserDentist.Email;

                        status = true;
                    }
                    #endregion
                }
                else
                {
                    message = "Invalid Request";
                }
                ViewBag.Message = message;
                ViewBag.Status = status;
                return PartialView(UserDentist);
            }
            catch (DbEntityValidationException e)
            {
                string errmessage = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    errmessage += ("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    errmessage += "\n";
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errmessage += ("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        errmessage += "\n";
                    }
                }
                ViewBag.Message = errmessage;
                return PartialView();
                //throw;
            }
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                var v = db.tbl_Dentist.Where(a => a.ActivationCode == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.IsEmailVerified = true;
                    db.SaveChanges();
                    status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
            }
            ViewBag.Status = status;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginDentist login, string ReturnUrl)
        {
            string message = "";
            bool status = false;
            if (ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var v = db.tbl_Dentist.Where(a => a.Username == login.Username).FirstOrDefault();
                    if (v != null)
                    {
                        if (string.Compare(Crypto.Hash(login.Password), v.Password) == 0)
                        {
                            int timeout = login.RememberMe ? 52600 : 20; //1 year
                            var ticket = new FormsAuthenticationTicket(login.Username, login.RememberMe, timeout);
                            string encrypted = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                            cookie.Expires = DateTime.Now.AddMinutes(timeout);
                            cookie.HttpOnly = true;
                            Response.Cookies.Add(cookie);

                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToActionPermanent("LoggedIndexDentist", "Home");
                            }

                        }
                        else
                        {
                            message = "Wrong Password";
                        }

                    }
                    else
                    {
                        message = "Username doesn't Exist";
                    }
                }
            }
            else
            {
                //message = "Invalid Request. Please check your input again.";
            }
            ViewBag.Message = message;
            return PartialView();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "UserDentist");
        }

        [NonAction]
        public bool IsEmailExist(string emailPatient)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var v = db.tbl_Dentist.Where(a => a.Email == emailPatient).FirstOrDefault();
                return v != null;
            }
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyurl = "/MyDentist/UserDentist/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);

            var fromEmail = new MailAddress("mydentistsystem@gmail.com", "My Dentist");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "batch7kelompok4";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is succesfully created";

                body = "<br/><br/>We are excited to tell you that your MyDentist account is" +
                " successfully created. Please click on the below link to verify your account" +
                " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/><br/>We got request for reset your account password. Please click on the below link to reset your password" +
                "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        //Forgot Password

        public ActionResult ForgotPassword()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            string message = "";
            //bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var account = db.tbl_Dentist.Where(a => a.Email == Email).FirstOrDefault();
                if (account != null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.Email, resetCode, "ResetPassword");
                    account.ResetPassword = resetCode;
                    //To avoid confirm password didnt match
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    message = "Reset Password has been sent to your email";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return PartialView();
        }

        public ActionResult ResetPassword(string id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var user = db.tbl_Dentist.Where(a => a.ResetPassword == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordPatient model = new ResetPasswordPatient();
                    model.ResetCode = id;
                    return PartialView(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordPatient model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var user = db.tbl_Dentist.Where(a => a.ResetPassword == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = Crypto.Hash(model.NewPassword);
                        user.ResetPassword = "";

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        message = "Password Updated succesfully";
                    }
                }
            }
            else
            {
                message = "Something Invalid";
            }

            ViewBag.Message = message;
            return View(model);
        }


        [Authorize]
        public ActionResult MyClinic()
        {
            bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "ALL COLUMN",
                    action_date = DateTime.Today,


                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();
                var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();

                if (d.IsEmailVerified==true)
                {
                    if (db.tbl_Schedule.Where(a => a.ID_Dentist == d.ID_Dentist).FirstOrDefault() != null)
                    {
                        var s = db.tbl_Schedule.Where(a => a.ID_Dentist == d.ID_Dentist).FirstOrDefault();
                        if (db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault() != null)
                        {
                            var c = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();
                            status = true;


                            ViewBag.Status = status;
                            return View(c);
                        }
                        else
                        {
                            ViewBag.Message = "Clinic doesn't exist. Please insert new clinic";
                            ViewBag.Status = status;
                            return View();
                        }

                    }
                    else
                    {
                        ViewBag.Message = "Clinic doesn't exist. Please insert new clinic";
                        ViewBag.Status = status;
                        return View();
                    } 
                }
                else
                {
                    string message = "Please Activate your account first";
                    ViewBag.Message = message;
                    ViewBag.activate = false;
                    return View();
                }



            }

        }
        [Authorize]
        public ActionResult Services(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var service = db.tbl_Services.Where(a => a.ID_Clinic == id).ToList();
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_Services",
                    column_name = "ALL COLUMN",
                    action_date = DateTime.Today,


                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();
                return PartialView(service);
            }
        }
        [Authorize]
        public ActionResult RemoveServices(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var s = db.tbl_Services.Where(a => a.ID_services == id).FirstOrDefault();
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();
                        db.tbl_Services.Remove(s);
                        db.SaveChanges();
                        dbTrans.Commit();
                        return RedirectToAction("Services", "UserDentist", new { id = s.ID_Clinic });
                    }
                    catch (DbEntityValidationException e)
                    {
                        var s = db.tbl_Services.Where(a => a.ID_services == id).FirstOrDefault();
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();
                        dbTrans.Rollback();
                        throw;
                    }
                }

                


            }
        }
        [Authorize]
        public ActionResult ClinicServices(int id, tbl_Services service)
        {
            bool status = false;
            string message = "";
            if (ModelState.IsValid)
            {


                service.ID_Clinic = id;
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    using (DbContextTransaction dbTran = db.Database.BeginTransaction())
                    {
                        try
                        {
                            tbl_Audit_Trail at = new tbl_Audit_Trail()
                            {
                                tbl_name = "tbl_Services",
                                column_name = "ALL COLUMN",
                                action_date = DateTime.Today,
                                new_value = service.Services_Name + "," + service.Services_Price.ToString(),


                                action_type = "CREATE"

                            };
                            db.tbl_Audit_Trail.Add(at);
                            db.SaveChanges();
                            db.tbl_Services.Add(service);
                            db.SaveChanges();
                            dbTran.Commit();
                        }
                        catch (Exception err)
                        {
                            dbTran.Rollback();
                            throw;
                        }
                    }


                }
                status = true;
                message = "Your New Service has been added successfully";

            }
            else
            {
                message = "Failed attempt. Please check your input again";

            }
            ViewBag.Message = message;
            ViewBag.Status = status;
            return PartialView(service);

        }
        [Authorize]
        public ActionResult InsertClinic(tbl_Clinic clinic)
        {
            string message = "";
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (ModelState.IsValid)
                        {
                            var dentist = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                            tbl_Schedule schedule = new tbl_Schedule();
                            HttpPostedFileBase file = Request.Files["ImageData"];
                            ClinicRepositories service = new ClinicRepositories();
                            int i = service.UploadImageInDataBase(file, clinic);
                            schedule.ID_Clinic = clinic.ID_Clinic;
                            schedule.ID_Dentist = dentist.ID_Dentist;
                            schedule.Start_hour = clinic.Open_hour;
                            schedule.End_hour = clinic.Close_hour;
                            db.tbl_Clinic.Add(clinic);
                            db.tbl_Schedule.Add(schedule);
                            db.Configuration.ValidateOnSaveEnabled = false;
                            db.SaveChanges();
                            dbtrans.Commit();
                            if (i == 1)
                            {
                                return RedirectToAction("MyClinic");
                            }
                        }



                        return View();
                    }
                    catch (Exception error)
                    {
                        message = error.Message;
                        dbtrans.Rollback();
                        return View();
                        throw;
                    }
                }

            }

        }


        [Authorize]
        public ActionResult MySchedule(int? page, string searching)
        {
            bool status = false;
            int pageNumber = (page ?? 1);
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                
                var ud = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();

                if (ud.IsEmailVerified==true)
                {
                    if (db.tbl_Schedule.Where(a => a.ID_Dentist == ud.ID_Dentist).FirstOrDefault() != null)
                    {
                        var sch = db.tbl_Schedule.Where(a => a.ID_Dentist == ud.ID_Dentist).FirstOrDefault();
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Reservation",
                            column_name = "ALL COLUMN",
                            action_date = DateTime.Today,


                            action_type = "READ"

                        };
                        db.tbl_Audit_Trail.Add(at);
                        db.SaveChanges();
                        status = true;
                        ViewBag.Status = status;
                        return PartialView(db.tbl_Reservation.Where(a => a.Clinic_ID == sch.ID_Clinic && a.Reservation_status != "Waiting Approval" && a.Reservation_status != "Finished").
                            OrderByDescending(t => t.ID_Reservation).ToList().ToPagedList(pageNumber, pageSize));
                    }
                    else
                    {
                        ViewBag.Status = status;
                        ViewBag.Message = "Please Add Your Clinic On 'My Clinic' Menu";
                        return PartialView();
                    } 
                }
                else
                {
                    string message = "Please Activate your account first";
                    ViewBag.Message = message;
                    ViewBag.activate = false;
                    return View();
                }
            }
        }

        [Authorize]
        public ActionResult ScheduleDetails(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                var c = db.tbl_Clinic.Where(a => a.ID_Clinic == r.Clinic_ID).FirstOrDefault();
                c.Current_Queue = r.Queue_number;
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_Reservation",
                    column_name = "ALL COLUMN",
                    action_date = DateTime.Today,


                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);

                db.SaveChanges();
                return View(r);
            }
        }
        public ActionResult NextPatient(int queue_number, int clinic_id)
        {
            string message = "";

            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var r = db.tbl_Reservation.Where(a => a.Clinic_ID == clinic_id && a.Queue_number == (queue_number + 1)).FirstOrDefault();
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == clinic_id).FirstOrDefault();
                        c.Current_Queue = queue_number + 1;
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Clinic",
                            column_name = "queue_number",
                            action_date = DateTime.Today,
                            new_value = c.Current_Queue.ToString(),

                            action_type = "UPDATE"

                        };
                        db.tbl_Audit_Trail.Add(at);

                        db.SaveChanges();
                        dbtran.Commit();
                        if (r != null)
                        {
                            ViewBag.Status = true;
                            return View(r);
                        }
                        else
                        {
                            ViewBag.Message = "There is no patient left";
                            return View();
                        }
                    }
                    catch (Exception)
                    {
                        dbtran.Rollback();
                        throw;
                    }
                }
            }
        }
        [Authorize]
        public ActionResult StartExamination(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                        //var p = db.tbl_Patient.Where(a => a.ID_Patient == r.ID_Patient).FirstOrDefault();
                        r.Reservation_status = "Ongoing";
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Reservation",
                            column_name = "Reservation_status",
                            action_date = DateTime.Today,
                            new_value = r.Reservation_status,

                            action_type = "UPDATE"

                        };
                        db.tbl_Audit_Trail.Add(at);
                        db.SaveChanges();
                        dbtran.Commit();
                        return View(r);
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        throw;
                    }
                }
            }
        }
        
        [Authorize]
        [HttpGet]
        public ActionResult AddNote(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                return View(r);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddNote(int id,tbl_Reservation reserve)
        {
            bool status = false;
            string message = "";
            if (ModelState.IsValid)
            {


                
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    
                    using (DbContextTransaction dbTran = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                            r.note = reserve.note;
                            reserve.Patient_Name = r.Patient_Name;
                            reserve.Queue_number = r.Queue_number;
                            reserve.Reservation_Date = r.Reservation_Date;
                            db.SaveChanges();
                            dbTran.Commit();
                            return RedirectToAction("StartExamination", "UserDentist", new { id = r.ID_Reservation });
                        }
                        catch (Exception err)
                        {
                            dbTran.Rollback();
                            throw;
                        }
                    }


                }
                status = true;
                message = "Your New Service has been added successfully";
                

            }
            else
            {
                message = "Failed attempt. Please check your input again";

            }
            ViewBag.Message = message;
            ViewBag.Status = status;
            return View(reserve);
        }
        [Authorize]
        public ActionResult ServiceList(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                var s = db.tbl_Services.Where(a => a.ID_Clinic == r.Clinic_ID).ToList();
                ViewBag.data = id;
                foreach (tbl_Services item in s)
                {
                    if (item.quantity == null)
                    {
                        item.quantity = 0;
                    }
                }
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_Reservation",
                    column_name = "Service_Name, Service_Price",
                    action_date = DateTime.Today,


                    action_type = "READ"

                };
                db.Configuration.ValidateOnSaveEnabled = false;
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();
                return View(s);
            }
        }
        [Authorize]
        public ActionResult Cart(int id)
        {
            try
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                    {
                        try
                        {
                            ViewBag.data = id;
                            int? total = 0;
                            var t = db.tbl_Transaction.Where(a => a.ID_Reservation == id).ToList();
                            foreach (tbl_Transaction item in t)
                            {
                                total += item.Services_Price * item.quantity;
                            }
                            tbl_Audit_Trail at = new tbl_Audit_Trail()
                            {
                                tbl_name = "tbl_Transaction",
                                column_name = "Service_Name, Service_Price,quantity",
                                action_date = DateTime.Today,


                                action_type = "READ"

                            };
                            db.Configuration.ValidateOnSaveEnabled = false;
                            db.tbl_Audit_Trail.Add(at);
                            db.SaveChanges();
                            ViewBag.Message = total;
                            dbtran.Commit();
                            return PartialView(t);
                        }
                        catch (DbEntityValidationException e)
                        {
                            string errmessage = e.Message;
                            dbtran.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                string errmessage = e.Message;
               
                throw;
            }
        }

        [Authorize]
        public ActionResult Checkout(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        int? total = 0;
                        var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                        var t = db.tbl_Transaction.Where(a => a.ID_Reservation == id).ToList();
                        foreach (tbl_Transaction i in t)
                        {
                            if (i.quantity != null || i.quantity != 0)
                            {
                                total += i.Services_Price * i.quantity;
                            }
                            else
                            {
                                total += i.Services_Price;
                            }
                        }
                        r.Price = total;
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Reservation",
                            column_name = "Price",
                            action_date = DateTime.Today,
                            new_value = r.Price.ToString(),

                            action_type = "UPDATE"

                        };
                        db.tbl_Audit_Trail.Add(at);

                        db.SaveChanges();
                        dbtran.Commit();
                        return View(r);
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        throw;
                    }
                }

            }
        }

        [Authorize]
        public ActionResult AddService(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var s = db.tbl_Services.Where(a => a.ID_services == id).FirstOrDefault();
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();
                        var r = db.tbl_Reservation.Where(a => a.Queue_number == c.Current_Queue && a.Reservation_status == "Ongoing").FirstOrDefault();
                        var item = db.tbl_Transaction.Where(a => a.ID_Reservation == r.ID_Reservation && a.ID_Services == s.ID_services).FirstOrDefault();
                        if (item == null)
                        {

                            tbl_Transaction t = new tbl_Transaction()
                            {
                                ID_Services = s.ID_services,
                                Services_Name = s.Services_Name,
                                Services_Price = s.Services_Price,
                                ID_Reservation = r.ID_Reservation,
                                Transaction_date = DateTime.Now,
                                quantity = 1

                            };
                            tbl_Audit_Trail at = new tbl_Audit_Trail()
                            {
                                tbl_name = "tbl_Transaction",
                                column_name = "ID_Services, Services_Name, Services_Price, ID_Reservation, Transaction_date, quantity",
                                action_date = DateTime.Today,
                                new_value = t.ID_Services + "," + t.Services_Name + "," + t.Services_Price.ToString() + "," + t.ID_Reservation + "," + t.Transaction_date.ToString()
                                + t.quantity.ToString(),

                                action_type = "CREATE"

                            };
                            db.Configuration.ValidateOnSaveEnabled = false;
                            db.tbl_Audit_Trail.Add(at);
                            db.tbl_Transaction.Add(t);
                            db.SaveChanges();
                            
                        }
                        else
                        {
                            if (item.quantity == null || item.quantity == 0)
                            {
                                item.quantity = 1;
                            }
                            item.quantity++;
                            tbl_Audit_Trail at = new tbl_Audit_Trail()
                            {
                                tbl_name = "tbl_Transaction",
                                column_name = "quantity",
                                action_date = DateTime.Today,
                                new_value = item.quantity.ToString(),


                                action_type = "UPDATE"

                            };
                            db.tbl_Audit_Trail.Add(at);
                            db.SaveChanges();
                        }
                        dbtran.Commit();
                        return RedirectToAction("Cart", "UserDentist", new { id = r.ID_Reservation });

    }
                    catch (DbEntityValidationException err)
                    {
                        var s = db.tbl_Services.Where(a => a.ID_services == id).FirstOrDefault();
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();
                        var r = db.tbl_Reservation.Where(a => a.Queue_number == c.Current_Queue && a.Reservation_status == "Ongoing").FirstOrDefault();
                        dbtran.Rollback();
                        return RedirectToAction("Cart", "UserDentist", new { id = r.ID_Reservation });
                        throw;
                    } 
                }

            }
        }
        
        public ActionResult RemoveItem(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var t = db.tbl_Transaction.Where(a => a.ID_transaction == id).FirstOrDefault();
                        if (t.quantity > 0)
                        {
                            t.quantity--;
                        }
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Transaction",
                            column_name = "quantity",
                            action_date = DateTime.Today,
                            new_value = t.quantity.ToString(),


                            action_type = "UPDATE"

                        };
                        db.tbl_Audit_Trail.Add(at);
                        db.SaveChanges();
                        dbtran.Commit();
                        return RedirectToAction("Cart", "UserDentist", new { id = t.ID_Reservation });
                    }
                    catch (Exception)
                    {
                        var t = db.tbl_Transaction.Where(a => a.ID_transaction == id).FirstOrDefault();
                        dbtran.Rollback();
                        return RedirectToAction("Cart", "UserDentist", new { id = t.ID_Reservation });
                        throw;
                    }
                }
            }
        }
        public ActionResult ExaminationDone(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                        var p = db.tbl_Patient.Where(a => a.ID_Patient == r.ID_Patient).FirstOrDefault();
                        r.Reservation_status = "Done";
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Reservation",
                            column_name = "Reservation_status",
                            action_date = DateTime.Today,
                            new_value = "Done",


                            action_type = "UPDATE"

                        };
                        SendReceiptMail(p.Email, r.Patient_Name, r, "Pay");
                        db.tbl_Audit_Trail.Add(at);
                        db.SaveChanges();
                        dbtran.Commit();
                        return View(r);
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        throw;
                        return View();
                    }
                }
            }
        }

        [NonAction]
        public void SendReceiptMail(string emailID, string name, tbl_Reservation r, string emailFor = "Reservation Success")
        {

            var fromEmail = new MailAddress("mydentistsystem@gmail.com", "My Dentist");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "batch7kelompok4";

            string subject = "";
            string body = "";
            if (emailFor == "Reservation Success")
            {
                subject = "Your Reservation is succesful";

                body = "<br/><br/>" + name + ", We hereby declare your order has been successfully made. Here are your order details" +
                "<br/><br/>Clinic name : " + r.Clinic_Name + "<br/><br/>Queue Number : " + r.Queue_number + "<br/><br/>Examination Date : " + r.Reservation_Date +
                " <br/><br/>Thank You.";
            }
            else if (emailFor == "Reminder")
            {
                subject = "Examination Reminder";
                body = "Hi,<br/>br/>Your checking will start after 1 more queue number. Please come to the clinic within 30 minutes";
            }
            else if (emailFor == "Start")
            {
                subject = "The Dentist is waiting for you";
                body = "Hi,<br/>br/>We hereby inform you that the clinic queue number has reached your queue number. Please enter the room for examination.<br/>br/>Thank You";
            }
            else if (emailFor == "Cancel")
            {
                subject = "Your examination has been canceled";
                body = "Hi,<br/><br/>We hereby inform you that your order with these details has been canceled " + "<br/><br/>Clinic name : "
                    + r.Clinic_Name + "<br/><br/>Queue Number : " + r.Queue_number + "<br/><br/>Examination Date : " + r.Reservation_Date + "<br/><br/>Thank You";

            }
            else if(emailFor == "Pay")
            {
                subject = "Your Payment is Waiting";
                body = "Hi,<br/><br/>We hereby inform you that your order with these details has been completed " + "<br/><br/>Clinic name : "
                    + r.Clinic_Name + "<br/><br/>Queue Number : " + r.Queue_number + "<br/><br/>Examination Date : " + r.Reservation_Date + "<br/><br/>Please Pay and upload your payment evidence to the MyDentist Website on MyReservation Menu<br/><br/>Thank You";

            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        [Authorize]
        public ActionResult DentistFAQ()
        {
            return PartialView();
        }

        public ActionResult PaidReservations()
        {
            return View();
        }

        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var q = from temp in db.tbl_Clinic where temp.ID_Clinic == Id select temp.Picture;
                byte[] cover = q.First();
                return cover;
            }

        }
        public ActionResult ShowPaymentPicture(int id)
        {
            byte[] cover = GetPaymentPicture(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }
        public byte[] GetPaymentPicture(int Id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var q = from temp in db.tbl_payment where temp.ID_Reservation == Id select temp.Picture;
                byte[] cover = q.First();
                return cover;
            }

        }
        [Authorize]
        [HttpGet]
        public ActionResult EditClinic(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                
                    var c = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
                    
                return View(c);
            }
                
        }
        
        [HttpPost]
        public ActionResult EditClinic(int id,tbl_Clinic clinic)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
                        c.Clinic_Name = clinic.Clinic_Name;
                        c.Address = clinic.Address;
                        c.Phone = clinic.Phone;
                        c.Open_hour = clinic.Open_hour;
                        c.Close_hour = clinic.Close_hour;
                        c.Patient_limit = clinic.Patient_limit;


                        HttpPostedFileBase file = Request.Files["ImageData"];
                        EditClinicRepositories services = new EditClinicRepositories();
                        int i = services.UploadImageInDataBase(file, c, c.ID_Clinic);
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        return View(c);
                    }
                    catch (DbEntityValidationException err)
                    {
                        var c = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
                        ViewBag.errmessage = err.Message;
                        throw;
                        return View(clinic);
                    } 
                }
            }
        }
        public ActionResult GetLocation(GetLocationPatient loc)
        {
            //Save latitude and longitude
            /*using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var current = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                current.RecentLatitude=
            }*/
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                var s = db.tbl_Schedule.Where(a => a.ID_Dentist == d.ID_Dentist).FirstOrDefault();
                var v = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();
                v.Latitude = Convert.ToDecimal(loc.lat);
                v.Longitude = Convert.ToDecimal(loc.lng);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return Json(v, JsonRequestBehavior.AllowGet);
            }
            //return Json(loc, JsonRequestBehavior.AllowGet);
        }
        
        [Authorize]
       [HttpGet]
        public ActionResult ClinicMap()
        {
            
            return View();
        }
        [Authorize]
        public ActionResult PaidReservation(int? page, string searching)
        {
            int pageNumber = (page ?? 1);
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                 
                var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                var s = db.tbl_Schedule.Where(a => a.ID_Dentist == d.ID_Dentist).FirstOrDefault();
                var r = db.tbl_Reservation.Where(a=>a.Clinic_ID==s.ID_Clinic && 
                a.Reservation_status== "Waiting Approval").ToList();
                return View(r.ToPagedList(pageNumber, pageSize));
            }
        }
        [Authorize]
        public ActionResult FinishedReservation(int? page, string searching)
        {
            int pageNumber = (page ?? 1);
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {

                var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                var s = db.tbl_Schedule.Where(a => a.ID_Dentist == d.ID_Dentist).FirstOrDefault();
                var r = db.tbl_Reservation.Where(a => a.Clinic_ID == s.ID_Clinic &&
                a.Reservation_status == "Finished").ToList();
                return View(r.ToPagedList(pageNumber, pageSize));
            }
        }
        [Authorize]
        public ActionResult PaidDetails(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var p = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                var pay = db.tbl_payment.Where(a => a.ID_Reservation == id).FirstOrDefault();
                ViewBag.bank_name = pay.bank_name;
                ViewBag.Account_owner = pay.Account_owner;
                ViewBag.Account_number = pay.Account_number;
                return View(p);
            }
            
        }
        [Authorize]
        public ActionResult ApprovePayment(int id)
        {
            using(db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                r.Reservation_status = "Approved";
                db.SaveChanges();
                return View(r);
            }
        }

        [HttpGet]
        public ActionResult EditProfile()
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                return View(d);
            }
        }
        [HttpPost]
        public ActionResult EditProfile(tbl_Dentist dentist)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                        if (dentist.Name != null)
                        {
                            d.Name = dentist.Name;
                        }

                        if (dentist.Phone != null)
                        {
                            d.Phone = dentist.Phone;
                        }

                        if (dentist.Address != null)
                        {
                            d.Address = dentist.Address;
                        }
                        if (dentist.Birth_date != null)
                        {
                            d.Birth_date = dentist.Birth_date;
                        }

                        HttpPostedFileBase file = Request.Files["ImageData"];
                        DentistRepositories services = new DentistRepositories();
                        if (file != null)
                        {
                            int i = services.UploadImageInDataBase(file, d, d.ID_Dentist);
                        }

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        dbtran.Commit();
                        return View(d);
                    }
                    catch (Exception)
                    {
                        dbtran.Rollback();
                        throw;
                    }
                }
            }
        }
        public ActionResult RetrieveDentistImage(int id)
        {
            byte[] cover = GetDentistImage(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetDentistImage(int Id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var q = from temp in db.tbl_Dentist where temp.ID_Dentist == Id select temp.Picture;
                byte[] cover = q.First();
                return cover;
            }

        }

        public ActionResult ProfileDentist()
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var d = db.tbl_Dentist.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                return View(d);
            }
        }
        [HttpGet]
        public ActionResult EditService(int id)
        {
            using(db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var s = db.tbl_Services.Where(a => a.ID_services == id).FirstOrDefault();
                return View(s);
            }
            
        }
        [HttpPost]
        public ActionResult EditService(int id, tbl_Services services)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using(DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            var s = db.tbl_Services.Where(a => a.ID_services == id).FirstOrDefault();
                            s.Services_Name = services.Services_Name;
                            s.Services_Price = services.Services_Price;
                            db.SaveChanges();
                            dbtran.Commit();
                            return RedirectToAction("Services", "UserDentist", new { id = s.ID_Clinic });
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        return View();
                        throw;
                    }

                }
            }
        }

    }
}