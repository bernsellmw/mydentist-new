﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MyDentist.Models;
using Microsoft.AspNet.Identity;
using System.Device.Location;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using MyDentist.Repositories;
using System.Data.Entity;
using PagedList;

namespace MyDentist.Controllers
{
    public class UserPatientController : Controller
    {
        private const int pageSize = 10;
        // GET: UserPatient
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude ="IsEmailVerified,ActivationCode")]tbl_Patient UserPatient)
        {
            try
            {
                bool status = false;
                string message = "";
                if (ModelState.IsValid)
                {
                    #region
                    var isExist = IsEmailExist(UserPatient.Email);
                    if (isExist)
                    {
                        ModelState.AddModelError("Email exist", "Email already Exist");
                        return PartialView(UserPatient);
                    }
                    #endregion

                    #region Generate Activation Code
                    UserPatient.ActivationCode = Guid.NewGuid();
                    #endregion


                    #region Password Hashing
                    UserPatient.Password = Crypto.Hash(UserPatient.Password);
                    UserPatient.ConfirmPassword = Crypto.Hash(UserPatient.ConfirmPassword);
                    #endregion
                    UserPatient.IsEmailVerified = false;

                    #region Save to Database
                    using (db_mydentistEntities1 db = new db_mydentistEntities1())
                    {
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Patient",
                            column_name = "ALL COLUMNS",
                            action_date = DateTime.Today,
                            new_value = UserPatient.ActivationCode + "," + UserPatient.Address + "," + UserPatient.Age + ","
                    + UserPatient.Birth_date + "," + UserPatient.Email + "," + UserPatient.Name + "," + UserPatient.Phone,

                            action_type = "CREATE"

                        };
                        db.tbl_Audit_Trail.Add(at);
                        db.tbl_Patient.Add(UserPatient);
                        db.SaveChanges();

                        //Send Email to user
                        SendVerificationLinkEmail(UserPatient.Email, UserPatient.ActivationCode.ToString());
                        message = "Registration successfully done. Account activation link " +
                    " has been sent to your email id:" + UserPatient.Email;

                        status = true;
                    }
                    #endregion
                }
                else
                {
                    message = "Invalid Request";
                }
                ViewBag.Message = message;
                ViewBag.Status = status;
                
                return PartialView(UserPatient);
            }
            catch (DbEntityValidationException e)
            {
                string errmessage = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    errmessage+=("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    errmessage += "\n";
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errmessage+=("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        errmessage += "\n";
                    }
                }
                ViewBag.Message = errmessage;
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    tbl_error_log el = new tbl_error_log()
                    {
                        error_type = "DbEntityValidationException",
                        error_message = errmessage,
                        table_name = "tbl_Patient",
                        column_name = "ALL COLUMN"
                    };
                    db.tbl_error_log.Add(el);
                    db.SaveChanges();
                }
                
                
                return PartialView();
                //throw;
            }
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                var v = db.tbl_Patient.Where(a => a.ActivationCode == new Guid(id)).FirstOrDefault();
                if (v!=null)
                {
                    v.IsEmailVerified = true;
                    db.SaveChanges();
                    status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
            }
            ViewBag.Status = status;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginPatient login, string ReturnUrl)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var v = db.tbl_Patient.Where(a => a.Username == login.Username).FirstOrDefault();
                    if (v != null)
                    {
                        if (string.Compare(Crypto.Hash(login.Password), v.Password) == 0)
                        {
                            int timeout = login.RememberMe ? 52600 : 20; //1 year
                            var ticket = new FormsAuthenticationTicket(login.Username, login.RememberMe, timeout);
                            string encrypted = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                            cookie.Expires = DateTime.Now.AddMinutes(timeout);
                            cookie.HttpOnly = true;
                            Response.Cookies.Add(cookie);

                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToActionPermanent("LoggedIndex", "Home");
                            }

                        }
                        else
                        {
                            message = "Wrong Password";
                        }

                    }
                    else
                    {
                        message = "Username doesn't Exist";
                    }
                } 
            }
            ViewBag.Message = message;
            return PartialView();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "UserPatient");
        }



        [NonAction]
        public bool IsEmailExist(string emailPatient)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var v = db.tbl_Patient.Where(a => a.Email == emailPatient).FirstOrDefault();
                return v != null;
            }
        }



        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode,string emailFor="VerifyAccount")
        {
            var verifyurl = "/MyDentist/UserPatient/"+emailFor+"/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);

            var fromEmail = new MailAddress("mydentistsystem@gmail.com", "My Dentist");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "batch7kelompok4";

            string subject = "";
            string body = "";
            if(emailFor=="VerifyAccount")
            {
                subject = "Your account is succesfully created";

                body = "<br/><br/>We are excited to tell you that your MyDentist account is" +
                " successfully created. Please click on the below link to verify your account" +
                " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor=="ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/>br/>We got request for reset your account password. Please click on the below link to reset your password" +
                "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }

            
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            }) 
                smtp.Send(message);
        }

        //Forgot Password

        public ActionResult ForgotPassword()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            string message = "";
            //bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var account = db.tbl_Patient.Where(a => a.Email == Email).FirstOrDefault();
                if(account!=null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.Email,resetCode,"ResetPassword");
                    account.ResetPassword = resetCode;
                    //To avoid confirm password didnt match
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    message = "Reset Password has been sent to your email";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return PartialView();
        }

        public ActionResult ResetPassword(string id)
        {
            using(db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var user = db.tbl_Patient.Where(a => a.ResetPassword == id).FirstOrDefault();
                if(user != null)
                {
                    ResetPasswordPatient model = new ResetPasswordPatient();
                    model.ResetCode = id;
                    return PartialView(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordPatient model)
        {
            var message = "";
            if(ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var user = db.tbl_Patient.Where(a => a.ResetPassword == model.ResetCode).FirstOrDefault();
                    if(user!=null)
                    {
                        user.Password = Crypto.Hash(model.NewPassword);
                        user.ResetPassword = "";

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        message = "Password Updated succesfully";
                    }
                }
            }
            else
            {
                message = "Something Invalid";
            }

            ViewBag.Message = message;
            return PartialView(model);
        }

       
        [Authorize]
        public ActionResult MyReservations(int? page, string searching)
        {
            int pageNumber = (page ?? 1);
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var user = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                if (user.IsEmailVerified==true)
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    return View(db.tbl_Reservation.Where(a => a.ID_Patient == user.ID_Patient && a.Reservation_status != "Canceled").OrderByDescending(r => r.ID_Reservation).ToList().ToPagedList(pageNumber, pageSize)); 
                }
                else
                {
                    string message = "Please Activate your account first";
                    ViewBag.Message = message;
                    ViewBag.activate = false;
                    return View();
                }
            }
        }

        public ActionResult CancelVerification(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();

                return View(r);
            }
        }
       
        [Authorize]
        public ActionResult CancelReservation(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                        var p = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();

                        var rnew = db.tbl_Reservation.Where(a => a.Clinic_ID == r.Clinic_ID && a.Reservation_Date == r.Reservation_Date &&
                        a.Queue_number > r.Queue_number).ToList();
                        foreach (tbl_Reservation item in rnew)
                        {
                            item.Queue_number--;
                        }
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_Reservation",
                            column_name = "ALL COLUMN",
                            action_date = DateTime.Today,


                            action_type = "DELETE"

                        };
                        db.tbl_Audit_Trail.Add(at);

                        db.tbl_Reservation.Remove(r);
                        db.SaveChanges();
                        dbtran.Commit();
                        SendReceiptMail(p.Email, p.Name, r, "Cancel");
                        ViewBag.Message = "Your Reservation has succesfuly canceled";
                        return View(r);
                    }
                    catch (Exception)
                    {
                        dbtran.Rollback();
                        throw;
                    }
                }
            }
        }

        [Authorize]
        public ActionResult NearbyClinics()
        {
            return View();
        }
        [Authorize]
        public ActionResult ShowClinics(int? page, string searching)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                int pageNumber = (page ?? 1);
                var p = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                GeoCoordinate location = new GeoCoordinate(Convert.ToDouble(p.RecentLatitude),Convert.ToDouble(p.RecentLongitude));

                var c = db.tbl_Clinic.ToList();
                var ordered = from d in c
                              let distance = new GeoCoordinate(Convert.ToDouble(d.Latitude), Convert.ToDouble(d.Longitude)).GetDistanceTo(location)
                              orderby distance
                              select d;
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "ALL COLUMN",
                    action_date = DateTime.Today,


                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();
                return View(ordered.ToPagedList(pageNumber, pageSize));
            }
        }
        [Authorize]
        public ActionResult PaymentDetails(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                var t = db.tbl_Transaction.Where(a => a.ID_Reservation == r.ID_Reservation).FirstOrDefault();
                int i = r.ID_Reservation;
                string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                ReportViewer report = new ReportViewer();
                report.ProcessingMode = ProcessingMode.Remote;
                report.Width = Unit.Pixel(1100);
                //report.SizeToReportContent = true;
                //report.AsyncRendering = true;
                report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
                ReportParameter[] reportParameter = new ReportParameter[1];
                reportParameter[0] = new ReportParameter("ID_Reservation", id.ToString());
                report.ServerReport.ReportPath = "/ReportMyDentistTransaction";
                report.ServerReport.SetParameters(reportParameter);
                report.ServerReport.Refresh();
                //report.ServerReport.ReportPath = "/ReportMyDentistReservation";
                ViewBag.ReportViewer = report;
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_Transaction",
                    column_name = "Service_Name, Service_Price, quantity, Transaction_Date",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                db.Configuration.ValidateOnSaveEnabled = false;
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();
                return View();
            }
        }
        [Authorize]
        public ActionResult ClinicDetails(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                string status = "";
                string message = "";
                var c = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
               if(db.tbl_Reservation.Where(a => a.Clinic_ID == id&&a.Reservation_Date==DateTime.Today).Max(a => a.Queue_number)<c.Patient_limit || db.tbl_Reservation.Where(a => a.Clinic_ID == id&&a.Reservation_Date==DateTime.Today).Max(a => a.Queue_number)==null)
                {
                    var patient = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                    if (db.tbl_Reservation.Where(a=>a.ID_Patient== patient.ID_Patient && a.Clinic_ID==id && a.Reservation_Date == DateTime.Today).FirstOrDefault()==null || db.tbl_Reservation.Where(a => a.ID_Patient == patient.ID_Patient && a.Clinic_ID == id).FirstOrDefault().Reservation_status=="Canceled")
                    {
                        status = "Available";
                        ViewBag.Status = status;
                    }
                    else
                    {
                        status = "Not Available";
                        ViewBag.Status = status;
                        ViewBag.Message = "You already booked this clinic";
                    }
                   
                }
               else
                {
                    status = "Not Available";
                    ViewBag.Status = status;
                    ViewBag.Message = "This clinic is already fully booked";
                }
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "Clinic_Name, Patient_Limit, Open_hour, Close_hour, Clinic_Address, Patient_Limit",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return View(c);
            }

        }
        [Authorize]
        public ActionResult BookDentist(int id, tbl_Reservation reserve)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var clinic = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
                        var patient = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();

                        reserve.Patient_Name = patient.Name;
                        reserve.ID_Patient = patient.ID_Patient;
                        reserve.Clinic_Name = clinic.Clinic_Name;
                        reserve.Clinic_ID = clinic.ID_Clinic;
                        reserve.Reservation_Date = DateTime.Today;


                        if (db.tbl_Reservation.Where(a => a.Clinic_ID == clinic.ID_Clinic && a.Reservation_Date == DateTime.Today).Max(a => a.Queue_number) == null)
                        {
                            reserve.Queue_number = 1;

                        }
                        else
                        {
                            reserve.Queue_number = db.tbl_Reservation.Where(a => a.Clinic_ID == clinic.ID_Clinic).Max(a => a.Queue_number) + 1;
                        }
                        tbl_Audit_Trail at = new tbl_Audit_Trail()
                        {
                            tbl_name = "tbl_reservation",
                            column_name = "Patient_Name, ID_Patient, Clinic_Name, Clinic_ID, Reservation_Date, Queue_number",
                            action_date = DateTime.Today,
                            new_value = reserve.Patient_Name + ", " + reserve.ID_Patient.ToString() + ", " + reserve.Clinic_Name + ", " +
                            reserve.Clinic_ID.ToString() + ", " + reserve.Reservation_Date.ToString() + ", " + reserve.Queue_number.ToString(),

                            action_type = "CREATE"

                        };
                        SendReceiptMail(patient.Email, patient.Name, reserve, "Reservation Success");
                        reserve.Reservation_status = "Waiting";
                        db.tbl_Reservation.Add(reserve);
                        db.tbl_Audit_Trail.Add(at);
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        dbtran.Commit();
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        ViewBag.errmessage = err.Message;
                        throw;
                    }
                    return View(reserve); 
                }
            }

        }
        [Authorize]
        public ActionResult ReservationHistory()
        {
            string user = HttpContext.User.Identity.Name;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "Clinic_Name, Patient_Limit, Open_hour, Close_hour, Clinic_Address, Patient_Limit",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                var p = db.tbl_Patient.Where(a => a.Username == user).FirstOrDefault();
                int id = p.ID_Patient;
                string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                ReportViewer report = new ReportViewer();
                report.ProcessingMode = ProcessingMode.Remote;
                report.Width = Unit.Pixel(1100);
                //report.SizeToReportContent = true;
                //report.AsyncRendering = true;
                report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
                ReportParameter[] reportParameter = new ReportParameter[1];
                reportParameter[0] = new ReportParameter("ID_Patient",id.ToString());
                report.ServerReport.ReportPath = "/ReportMyDentistReservation";
                report.ServerReport.SetParameters(reportParameter);
                report.ServerReport.Refresh();
                //report.ServerReport.ReportPath = "/ReportMyDentistReservation";
                ViewBag.ReportViewer = report;
                return View();
            }
        }

        [NonAction]
        public void SendReceiptMail(string emailID, string name, tbl_Reservation r, string emailFor = "Reservation Success")
        {

            var fromEmail = new MailAddress("mydentistsystem@gmail.com", "My Dentist");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "batch7kelompok4";

            string subject = "";
            string body = "";
            if (emailFor == "Reservation Success")
            {
                subject = "Your Reservation is succesful";

                body = "<br/><br/>"+name+", We hereby declare your order has been successfully made. Here are your order details" +
                "<br/><br/>Clinic name : " + r.Clinic_Name + "<br/><br/>Queue Number : " + r.Queue_number+ "<br/><br/>Examination Date : " + r.Reservation_Date+
                " <br/><br/>Thank You.";
            }
            else if (emailFor == "Reminder")
            {
                subject = "Examination Reminder";
                body = "Hi,<br/>br/>Your checking will start after 1 more queue number. Please come to the clinic within 30 minutes" ;
            }
            else if (emailFor == "Start")
            {
                subject = "The Dentist is waiting for you";
                body = "Hi,<br/>br/>We hereby inform you that the clinic queue number has reached your queue number. Please enter the room for examination.<br/>br/>Thank You";
            }
            else if (emailFor == "Cancel")
            {
                subject = "Your examination has been canceled";
                body = "Hi,<br/><br/>We hereby inform you that your order with these details has been canceled "+"<br/><br/>Clinic name : " 
                    + r.Clinic_Name + "<br/><br/>Queue Number : " + r.Queue_number + "<br/><br/>Examination Date : " + r.Reservation_Date + "<br/><br/>Thank You";
               
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        [Authorize]
        public ActionResult PatientFAQ()
        {
            return PartialView();
        }
        [Authorize]
        public ActionResult ProfileDetail()
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                string username = User.Identity.Name;

                // Fetch the userprofile
                tbl_Patient profile = db.tbl_Patient.FirstOrDefault(u => u.Username.Equals(username));

                // Construct the viewmodel
                tbl_Patient model = new tbl_Patient();
                model.Address = profile.Address;
                model.Username = profile.Username;
                model.Name = profile.Name;
                model.Phone = profile.Phone;
                model.Email = profile.Email;
                model.Birth_date = profile.Birth_date;
                model.Password = profile.Password;
                model.ConfirmPassword = profile.ConfirmPassword;
                model.Picture = profile.Picture;
                return View(model);
                //var profile = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                //return View(profile);


            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult ProfileDetail(tbl_Patient profile)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_patient",
                    column_name = "ALL COLUMN",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };

                //db.Entry(profile).State = EntityState.Modified;
                //db.Configuration.ValidateOnSaveEnabled = false;
                //db.SaveChanges();
                //return View();
                if (ModelState.IsValid)
                {
                    
                    string username = User.Identity.Name;
                    // Get the userprofile\
                    var user = db.tbl_Patient.FirstOrDefault(u => u.Username.Equals(username));


                    // Update fields
                    user.Address = profile.Address;
                    user.Username = profile.Username;
                    user.Name = profile.Name;
                    user.Email = profile.Email;
                    user.Phone = profile.Phone;
                    user.Birth_date = profile.Birth_date;
                    user.Picture = profile.Picture;
                    user.Password = Crypto.Hash(profile.Password);
                    user.ConfirmPassword = Crypto.Hash(profile.ConfirmPassword);

                    //user.Picture = profile.Picture;
                    db.Entry(user).State = EntityState.Modified;

                     db.SaveChanges();

//return RedirectToAction("Index", "Home"); // or whatever


                     try
                     {
                         HttpPostedFileBase file = Request.Files["ImageData"];
                         ProfileRepositories service = new ProfileRepositories();
                         int i = service.UploadImageInDataBase(file, user);
                        // if (i == 1)
                         //{
                         //    return RedirectToAction("Index","Home");
                         //}


                         return View(user);
                     }
                     catch
                     {
                         return View();
                     }
                }

                return View();



            }
        }
        [Authorize]
        [HttpGet]
        public ActionResult Feedback(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                var c = db.tbl_Clinic.Where(a => a.ID_Clinic == r.Clinic_ID).FirstOrDefault();
                var s = db.tbl_Schedule.Where(a => a.ID_Clinic == c.ID_Clinic).FirstOrDefault();
                var d = db.tbl_Dentist.Where(a => a.ID_Dentist == s.ID_Dentist).FirstOrDefault();
                ViewBag.docName = d.Name;
                ViewBag.clinicName = c.Clinic_Name;
            }
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult Feedback( int id, tbl_Feedback feedback)
        {
            bool status = false;
            if(ModelState.IsValid)
            {
                
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                            var c = db.tbl_Clinic.Where(a => a.ID_Clinic == r.Clinic_ID).FirstOrDefault();
                            var s = db.tbl_Schedule.Where(a => a.ID_Clinic == c.ID_Clinic).FirstOrDefault();
                            var d = db.tbl_Dentist.Where(a => a.ID_Dentist == s.ID_Dentist).FirstOrDefault();
                            tbl_Feedback fb = new tbl_Feedback()
                            {
                                ID_Reservation = id,
                                FB_Date = DateTime.Now,
                                Score = feedback.Score,
                                Comment = feedback.Comment,
                                ID_Clinic = r.Clinic_ID
                            };
                            r.Reservation_status = "Finished";
                            ViewBag.Message = "Your feedback has been saved. Thank you for using our service";
                            db.tbl_Feedback.Add(fb);
                            db.SaveChanges();
                            dbtran.Commit();
                            status = true;
                            
                            ViewBag.Status = status;
                        }
                        catch (DbEntityValidationException err)
                        {
                            ViewBag.errmessage = err.Message;
                            dbtran.Rollback();
                            throw;
                        } 
                    }
                }
            }
            else
            {
                ViewBag.Message = "Score can't be empty";
                ViewBag.Status = status;
            }
            return View();
        }
        [Authorize]
        [HttpGet]
        public ActionResult UploadPayment(int id)
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult UploadPayment(tbl_payment pay, int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {

                        var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();

                        tbl_payment p = new tbl_payment();
                        HttpPostedFileBase file = Request.Files["ImageData"];
                        PaymentRepositories service = new PaymentRepositories();
                        //proses upload image di PaymentRepositories
                        pay.ID_Reservation = id;
                        pay.price = r.Price;
                        pay.IsPaid = false;
                        int i = service.UploadImageInDataBase(file, pay);
                        r.Reservation_status = "Waiting Approval";
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        dbtran.Commit();
                        if (i == 1)
                        {
                            return RedirectToAction("ConfirmPayment", "UserPatient", new { id = pay.ID_Reservation });
                        }

                        return View();
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        throw;
                        return View();
                    } 
                }
            }
        }
        [HttpGet]
        public ActionResult ConfirmPayment(int id)
        {
            tbl_payment  bankDrop = new tbl_payment ();
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                bankDrop = db.tbl_payment.Where(a => a.ID_Reservation == id).FirstOrDefault();
                bankDrop.BankList = db.tbl_bank.ToList<tbl_bank>();
                
                return View(bankDrop);
            }
        }
        [HttpPost]
        public ActionResult ConfirmPayment(int id, tbl_payment pay)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var p = db.tbl_payment.Where(a => a.ID_Reservation == id).FirstOrDefault();
                        p.bank_name = pay.bank_name;
                        p.Account_owner = pay.Account_owner;
                        p.Account_number = pay.Account_number;
                        p.BankList = db.tbl_bank.ToList<tbl_bank>();

                        HttpPostedFileBase file = Request.Files["ImageData"];
                        ConfirmPaymentRepositories services = new ConfirmPaymentRepositories();
                        int i = services.UploadImageInDataBase(file, p, p.ID_Payment);
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        dbtran.Commit();
                        return View(p);
                    }
                    catch (Exception err)
                    {
                        ViewBag.errmessage = err.Message;
                        dbtran.Rollback();
                        throw;
                        return View();
                    } 
                }
            }

        }

        [Authorize]
        public ActionResult RetrieveImagePayment(int id)
        {
            byte[] cover = GetImageFromDataBasePayment(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }
        [Authorize]
        public byte[] GetImageFromDataBasePayment(int Id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var q = from temp in db.tbl_payment where temp.ID_Payment == Id select temp.Picture;
                byte[] cover = q.First();
                return cover;
            }
        }

        public ActionResult ExTable()
        {
            return View();
        }
        [Authorize]
        public ActionResult RetrieveImageClinic(int id)
        {
            byte[] cover = GetImageFromDataBaseClinic(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
             }
             else
             {
                return null;
            }
         }
        [Authorize]
        public byte[] GetImageFromDataBaseClinic(int Id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var q = from temp in db.tbl_Clinic where temp.ID_Clinic == Id select temp.Picture;
                byte[] cover = q.First();
                return cover;
            }
        }

        [HttpGet]
        public JsonResult GetPatient()
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var user = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                db.Configuration.ValidateOnSaveEnabled = false;
                var contentModel = db.tbl_Reservation.Where(a => a.ID_Patient == user.ID_Patient).OrderByDescending(a => a.ID_Patient).ToList();
                return Json(contentModel, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize]
        [HttpGet]
        public ActionResult EditProfile()
        {
            using(db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var p = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                return View(p);
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult EditProfile(tbl_Patient patient)
        {
            using(db_mydentistEntities1 db = new db_mydentistEntities1())
            {

                using (DbContextTransaction dbtran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var p = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                        p.Name = patient.Name;

                        p.Phone = patient.Phone;
                        p.Address = patient.Address;
                        p.Birth_date = patient.Birth_date;
                        p.Picture = patient.Picture;

                        HttpPostedFileBase file = Request.Files["ImageData"];
                        PatientRepositories services = new PatientRepositories();
                        int i = services.UploadImageInDataBase(file, p, p.ID_Patient);
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        dbtran.Commit();
                        if (i == 1)
                        {
                            return RedirectToAction("ProfilePatient");
                        }
                        return View(p);
                    }
                    catch (DbEntityValidationException err)
                    {
                        dbtran.Rollback();
                        throw;
                        return View(patient);
                    }
                    
                }
               
            }
        }
        [Authorize]
        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }
        [Authorize]
        public byte[] GetImageFromDataBase(int Id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var q = from temp in db.tbl_Patient where temp.ID_Patient == Id select temp.Picture;
                byte[] cover = q.First();
                return cover;
            }

        }
        [Authorize]
        public ActionResult ProfilePatient()
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var p = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                return View(p);
            }
        }

        [Authorize]
        public ActionResult ReservationDetails(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var r = db.tbl_Reservation.Where(a => a.ID_Reservation == id).FirstOrDefault();
                var c = db.tbl_Clinic.Where(a => a.ID_Clinic == r.Clinic_ID).FirstOrDefault();
                ViewBag.CurrentQueue = c.Current_Queue;
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_Reservation",
                    column_name = "ALL COLUMN",
                    action_date = DateTime.Today,


                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);

                db.SaveChanges();
                return View(r);
            }
        }
        [Authorize]
        public ActionResult ShowService(int id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var c = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
                var s = db.tbl_Services.Where(a => a.ID_Clinic == id).ToList();
                ViewBag.Id_clinic = id;
                return View(s);
            }
        }

    }

}