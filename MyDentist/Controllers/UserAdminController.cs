﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MyDentist.Models;
using Microsoft.AspNet.Identity;
using System.Device.Location;
using Microsoft.Reporting.WebForms;
using System.Configuration;
using System.Web.UI.WebControls;

namespace MyDentist.Controllers
{
    public class UserAdminController : Controller
    {
        // GET: UserAdmin
        public ActionResult Index()
        {
            return View();
        }
        
       
        [HttpGet]
        public ActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginAdmin login, string ReturnUrl)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var v = db.tbl_Admin.Where(a => a.Username == login.Username).FirstOrDefault();
                    if (v != null)
                    {
                        if (string.Compare(login.Password, v.Password) == 0)
                        {
                            int timeout = login.RememberMe ? 52600 : 20; //1 year
                            var ticket = new FormsAuthenticationTicket(login.Username, login.RememberMe, timeout);
                            string encrypted = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                            cookie.Expires = DateTime.Now.AddMinutes(timeout);
                            cookie.HttpOnly = true;
                            Response.Cookies.Add(cookie);

                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToActionPermanent("LoggedIndexAdmin", "Home");
                            }

                        }
                        else
                        {
                            message = "Wrong Password";
                        }

                    }
                    else
                    {
                        message = "Username doesn't Exist";
                    }
                }
            }
            ViewBag.Message = message;
            return PartialView();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "UserAdmin");
        }



        [NonAction]
        public bool IsEmailExist(string emailPatient)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var v = db.tbl_Admin.Where(a => a.Email == emailPatient).FirstOrDefault();
                return v != null;
            }
        }



        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyurl = "/UserAdmin/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);

            var fromEmail = new MailAddress("mydentistsystem@gmail.com", "My Dentist");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "batch7kelompok4";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is succesfully created";

                body = "<br/><br/>We are excited to tell you that your MyDentist account is" +
                " successfully created. Please click on the below link to verify your account" +
                " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/>br/>We got request for reset your account password. Please click on the below link to reset your password" +
                "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        //Forgot Password

        /*public ActionResult ForgotPassword()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            string message = "";
            //bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var account = db.tbl_Admin.Where(a => a.Email == Email).FirstOrDefault();
                if (account != null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.Email, resetCode, "ResetPassword");
                    account.ResetPassword = resetCode;
                    //To avoid confirm password didnt match
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    message = "Reset Password has been sent to your email";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return PartialView();
        }

        public ActionResult ResetPassword(string id)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var user = db.tbl_Admin.Where(a => a.ResetPassword == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordPatient model = new ResetPasswordPatient();
                    model.ResetCode = id;
                    return PartialView(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordPatient model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var user = db.tbl_Admin.Where(a => a.ResetPassword == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = Crypto.Hash(model.NewPassword);
                        user.ResetPassword = "";

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        message = "Password Updated succesfully";
                    }
                }
            }
            else
            {
                message = "Something Invalid";
            }

            ViewBag.Message = message;
            return PartialView(model);
        }*/
        [Authorize]
        public ActionResult UserList()
        {
            return View();
        }
        [Authorize]
        public ActionResult DentistList()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;


            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/ReportMyDentistUserDentist";
            ViewBag.ReportViewer = report;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_Dentist",
                    column_name = "All Column",
                    action_date = DateTime.Today,
                    
                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();
                
            }
                return PartialView();
        }
        [Authorize]
        public ActionResult PatientList()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;


            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/ReportMyDentistPatientUser";
            ViewBag.ReportViewer = report;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_patient",
                    column_name = "All Column",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.SaveChanges();

            }
            return PartialView();
        }
        [Authorize]
        public ActionResult ClinicList()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;


            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/ReportMyDentistClinic";
            ViewBag.ReportViewer = report;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "Clinic_Name, Patient_Limit, Open_hour, Close_hour",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();

            }
            return PartialView();
        }
        [Authorize]
        public ActionResult AuditTrail()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;


            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/ReportMyDentistAuditTrail";
            ViewBag.ReportViewer = report;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "Clinic_Name, Patient_Limit, Open_hour, Close_hour",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();

            }
            return PartialView();
        }
        [Authorize]
        public ActionResult ErrorLog()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;


            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/ReportMyDentistErrorLog";
            ViewBag.ReportViewer = report;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                tbl_Audit_Trail at = new tbl_Audit_Trail()
                {
                    tbl_name = "tbl_clinic",
                    column_name = "Clinic_Name, Patient_Limit, Open_hour, Close_hour",
                    action_date = DateTime.Today,

                    action_type = "READ"

                };
                db.tbl_Audit_Trail.Add(at);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();

            }
            return PartialView();
        }
    }
}