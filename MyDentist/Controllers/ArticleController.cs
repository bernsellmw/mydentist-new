﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyDentist.Repositories;
using PagedList;

namespace MyDentist.Controllers
{
    [RoutePrefix("Index")]
    [ValidateInput(false)]
    public class ArticleController : Controller
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        private const int pageSize = 5;
        // GET: Article
        [Authorize]
        public ActionResult Index(int? page, string searching)
        {
            int pageNumber = (page ?? 1);
            return View((db.tbl_Article.Where(x => x.Tittle.Contains(searching) || searching == null)).OrderByDescending(s => s.ID_Article).ToList().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult IndexDentist(int? page, string searching)
        {
            int pageNumber = (page ?? 1);
            return PartialView((db.tbl_Article.Where(x => x.Tittle.Contains(searching) || searching == null)).OrderByDescending(s => s.ID_Article).ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: Article/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {
            return View(db.tbl_Article.Where(x => x.ID_Article == id).FirstOrDefault());
        }

        [Authorize]
        public ActionResult DetailsDentist(int id)
        {
            return PartialView(db.tbl_Article.Where(x => x.ID_Article == id).FirstOrDefault());
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Article/Create
        [Route("Create")]
        [HttpPost]
        [Authorize]
        public ActionResult Create(ArticleViewModel model)
        {
            try
            {
                HttpPostedFileBase file = Request.Files["ImageData"];
                ArticleRepositories service = new ArticleRepositories();
                int i = service.UploadImageInDataBase(file, model);
                if (i == 1)
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View();
            }
        }

        // GET: Article/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Article/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Article/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Article/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            var q = from temp in db.tbl_Article where temp.ID_Article == Id select temp.Picture;
            byte[] cover = q.First();
            return cover;
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [HttpGet]
        public JsonResult GetArticle()
        {
            var content = db.tbl_Article.Select(s => new
            {
                s.ID_Article,
                s.Tittle,
                s.Picture,
                s.Content,
                s.Release_date,
                s.Writer_name
            });

            List<ArticleViewModel> contentModel = content.Select(item => new ArticleViewModel()
            {
                ID_Article = item.ID_Article,
                Tittle = item.Tittle,
                Picture = item.Picture,
                Content = item.Content,
                Release_date = item.Release_date,
                Writer_name = item.Writer_name
            }).OrderByDescending(s => s.ID_Article).ToList();
            return Json(contentModel, JsonRequestBehavior.AllowGet);
        }
    }
}
