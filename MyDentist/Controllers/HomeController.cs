﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using MyDentist.Models;
using System.Device.Location;
using MyDentist.Repositories;




namespace MyDentist.Controllers
{
    public class HomeController : Controller
    {
        db_mydentistEntities1 dc = new db_mydentistEntities1();
        string activeUser = "";
        [Authorize]
        public ActionResult Index()
        {
            var content = dc.tbl_Article.Select(s => new
            {
                s.ID_Article,
                s.Tittle,
                s.Picture,
                s.Content,
                s.Release_date,
                s.Writer_name
            });

            List<ArticleViewModel> contentModel = content.Select(item => new ArticleViewModel()
            {
                ID_Article = item.ID_Article,
                Tittle = item.Tittle,
                Picture = item.Picture,
                Content = item.Content,
                Release_date = item.Release_date,
                Writer_name = item.Writer_name
            }).OrderByDescending(s => s.ID_Article).Take(3).ToList();
            return View(contentModel);
        }

        [Authorize]
        public ActionResult IndexDentist()
        {
            var content = dc.tbl_Article.Select(s => new
            {
                s.ID_Article,
                s.Tittle,
                s.Picture,
                s.Content,
                s.Release_date,
                s.Writer_name
            });

            List<ArticleViewModel> contentModel = content.Select(item => new ArticleViewModel()
            {
                ID_Article = item.ID_Article,
                Tittle = item.Tittle,
                Picture = item.Picture,
                Content = item.Content,
                Release_date = item.Release_date,
                Writer_name = item.Writer_name
            }).OrderByDescending(s => s.ID_Article).Take(3).ToList();
            return PartialView(contentModel);
        }

        [Authorize]
        public ActionResult IndexAdmin()
        {
            var content = dc.tbl_Article.Select(s => new
            {
                s.ID_Article,
                s.Tittle,
                s.Picture,
                s.Content,
                s.Release_date,
                s.Writer_name
            });

            List<ArticleViewModel> contentModel = content.Select(item => new ArticleViewModel()
            {
                ID_Article = item.ID_Article,
                Tittle = item.Tittle,
                Picture = item.Picture,
                Content = item.Content,
                Release_date = item.Release_date,
                Writer_name = item.Writer_name
            }).OrderByDescending(s => s.ID_Article).Take(3).ToList();
            return PartialView(contentModel);
        }

        [Authorize]
        public ActionResult About()
        {

            return View();
        }
        [Authorize]
        public ActionResult HowTo()
        {

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {


            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [Authorize]
        public ActionResult LoggedIndex()
        {
            activeUser = HttpContext.User.Identity.Name;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var u = db.tbl_Patient.Where(a => a.Username == activeUser).FirstOrDefault();
                if (u.IsEmailVerified==true)
                {
                    if (u != null)
                    {
                        ViewBag.Message = u.Name;
                        DashboardPatient dp = new DashboardPatient();
                        dp.ReservationMade = db.tbl_Reservation.Where(a => a.ID_Patient == u.ID_Patient).Count();
                        dp.LastReservation = db.tbl_Reservation.Where(a => a.ID_Patient == u.ID_Patient).Max(p => p.Reservation_Date);
                        dp.NotPaid = db.tbl_Reservation.Where(a => a.ID_Patient == u.ID_Patient && a.Reservation_status == "Done").Count();
                        return View(dp);
                    }
                    else
                    {
                        return RedirectToActionPermanent("LoggedIndexDentist", "Home");
                    } 
                }
                else
                {
                    string message = "Please Activate your account first";
                    ViewBag.Message = message;
                    ViewBag.activate = false;
                    return View();
                }
            }
            
        }

        [Authorize]
        public ActionResult LoggedIndexDentist()
        {
           
            activeUser = HttpContext.User.Identity.Name;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                bool status = false;
                
                var u = db.tbl_Dentist.Where(a => a.Username == activeUser).FirstOrDefault();
                if (u != null)
                {

                    if (u.IsEmailVerified==true)
                    {
                        if (db.tbl_Schedule.Where(a => a.ID_Dentist == u.ID_Dentist).FirstOrDefault() != null)
                        {
                            var s = db.tbl_Schedule.Where(a => a.ID_Dentist == u.ID_Dentist).FirstOrDefault();
                            var c = db.tbl_Clinic.Where(a => a.ID_Clinic == s.ID_Clinic).FirstOrDefault();

                            DashboardDentistcs dt = new DashboardDentistcs()
                            {
                                PatientToday = db.tbl_Reservation.Where(a => a.Clinic_ID == c.ID_Clinic && a.Reservation_Date == DateTime.Today).Count(),
                                income = db.tbl_Reservation.Where(a => a.Clinic_ID == c.ID_Clinic && (a.Reservation_status == "Approved" || a.Reservation_status == "Finished")).Sum(a => a.Price),


                            };
                            int? t = db.tbl_Feedback.Where(a => a.ID_Clinic == s.ID_Clinic).Sum(a => a.Score);
                            int? tc = db.tbl_Feedback.Where(a => a.ID_Clinic == s.ID_Clinic).Count();
                            int? score = t / tc;
                            if (score == 1)
                            {
                                dt.ClinicScore = "Bad";
                            }
                            else if (score == 2)
                            {
                                dt.ClinicScore = "Not Bad";
                            }
                            else if (score == 3)
                            {
                                dt.ClinicScore = "Average";
                            }
                            else if (score == 4)
                            {
                                dt.ClinicScore = "Good";
                            }
                            else if (score == 5)
                            {
                                dt.ClinicScore = "Very Good";
                            }
                            else
                            {
                                dt.ClinicScore = "Not Rated";
                            }
                            if (dt.income == null)
                            {
                                dt.income = 0;
                            }
                            if (dt.ClinicScore == null)
                            {
                                dt.ClinicScore = "Not Scored";
                            }
                            ViewBag.Message = activeUser;
                            status = true;
                            ViewBag.Status = true;
                            return View(dt);
                        }
                        else
                        {
                            string message = "Please Add Your Clinic On 'My Clinic' Menu";
                            ViewBag.Message = message;
                            return View();
                        } 
                    }
                    else
                    {
                        string message = "Please Activate your account first";
                        ViewBag.Message = message;
                        ViewBag.activate = false;
                        return View();
                    }
                }
                else
                {
                    return RedirectToActionPermanent("LoggedIndex", "Home");
                }
            }
            
        }

        [Authorize]
        public ActionResult LoggedIndexAdmin()
        {
            
            activeUser = HttpContext.User.Identity.Name;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var u = db.tbl_Admin.Where(a => a.Username == activeUser);
                if (u != null)
                {
                    DashboardAdmin da = new DashboardAdmin()
                    {
                        Clinics = db.tbl_Clinic.Count(),
                        DentistUser = db.tbl_Dentist.Count(),
                        PatientUser = db.tbl_Patient.Count(),
                        ReservationMade = db.tbl_Reservation.Count(),
                        ReservationsToday = db.tbl_Reservation.Where(a => a.Reservation_Date == DateTime.Today).Count()
                    };
                    ViewBag.Message = activeUser;
                    return View(da);
                }
                else
                {
                    return RedirectToActionPermanent("LoggedIndex", "Home");
                }
            }

        }

        [Authorize]
        [HttpGet]
        public ActionResult Gmap()
        {
            ViewBag.Message = HttpContext.User.Identity.Name;
            return View();
        }

        
        
        public ActionResult GetData(tbl_Patient patient)
        {
            activeUser = HttpContext.User.Identity.Name;
            
            
                    patient = dc.tbl_Patient.Where(a => a.Username == activeUser).FirstOrDefault();
                    return Json(patient, JsonRequestBehavior.AllowGet);

        }

        
        public ActionResult GetLocation(GetLocationPatient loc)
        {
            //Save latitude and longitude
            /*using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var current = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                current.RecentLatitude=
            }*/
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var v = db.tbl_Patient.Where(a => a.Username == HttpContext.User.Identity.Name).FirstOrDefault();
                v.RecentLatitude = Convert.ToDecimal(loc.lat);
                v.RecentLongitude = Convert.ToDecimal(loc.lng);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return Json(v, JsonRequestBehavior.AllowGet);
            }
                //return Json(loc, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            var q = from temp in dc.tbl_Article where temp.ID_Article == Id select temp.Picture;
            byte[] cover = q.First();
            return cover;
        }

        public ActionResult IndexArticleKanan()
        {
            var artikel = dc.tbl_Article.Select(s => new
            {
                s.ID_Article,
                s.Tittle,
                s.Picture,
                s.Content,
                s.Release_date,
                s.Writer_name
            }).Take(2);

            List<ArticleViewModel> artikelModel = artikel.Select(item => new ArticleViewModel()
            {
                ID_Article = item.ID_Article,
                Tittle = item.Tittle,
                Picture = item.Picture,
                Content = item.Content,
                Release_date = item.Release_date,
                Writer_name = item.Writer_name
            }).ToList();
            return View(artikelModel);
        }
    }
}