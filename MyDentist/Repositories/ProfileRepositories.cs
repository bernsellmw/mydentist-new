﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace MyDentist.Repositories
{
    public class ProfileRepositories
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_Patient contentViewModel)
        {

            contentViewModel.Picture = ConvertToBytes(file);
            var Content = new tbl_Patient
            {
                Address = contentViewModel.Address,
                Username = contentViewModel.Username,
                Name = contentViewModel.Name,
                Email = contentViewModel.Email,
                Phone = contentViewModel.Phone,
                Birth_date = contentViewModel.Birth_date,
                Password = contentViewModel.Password,
                ConfirmPassword = contentViewModel.ConfirmPassword,
                Picture = contentViewModel.Picture,

            };

            //db.tbl_Patient.Add(Content);
            db.Entry(Content).State = EntityState.Modified;
            db.SaveChanges();
            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}