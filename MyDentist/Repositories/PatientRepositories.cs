﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
namespace MyDentist.Repositories
{
    public class PatientRepositories
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_Patient contentViewModel,int id)
        {
            if(file!=null)
            {
                contentViewModel.Picture = ConvertToBytes(file);
            }
            else
            {
                contentViewModel.Picture = null;
            }
            
            
            var p = db.tbl_Patient.Where(a => a.ID_Patient == id).FirstOrDefault();
            p.Name = contentViewModel.Name;

            p.Phone = contentViewModel.Phone;
            p.Address = contentViewModel.Address;
            p.Birth_date = contentViewModel.Birth_date;
            p.Picture = contentViewModel.Picture;
            
            db.Configuration.ValidateOnSaveEnabled = false;

            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}