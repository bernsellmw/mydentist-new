﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MyDentist.Repositories
{
    public class ClinicRepositories
    {
        private readonly db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_Clinic contentViewModel)
         {
            contentViewModel.Picture = ConvertToBytes(file);
            var Content = new tbl_Clinic
            {
                Address = contentViewModel.Address,
                Phone = contentViewModel.Phone,
                Open_hour = contentViewModel.Open_hour,
                Close_hour = contentViewModel.Close_hour,
                Patient_limit = contentViewModel.Patient_limit,
                Clinic_Name = contentViewModel.Clinic_Name,
                Picture = contentViewModel.Picture
            };

            db.tbl_Clinic.Add(Content);
            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }

    }
}