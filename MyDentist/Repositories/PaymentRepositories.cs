﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MyDentist.Repositories
{
    public class PaymentRepositories
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_payment contentViewModel)
        {

            contentViewModel.Picture = ConvertToBytes(file);
            var Content = new tbl_payment
            {
                ID_Reservation = contentViewModel.ID_Reservation,
                Picture = contentViewModel.Picture,
                IsPaid = contentViewModel.IsPaid,
                price =  contentViewModel.price

            };

            db.tbl_payment.Add(Content);
            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}