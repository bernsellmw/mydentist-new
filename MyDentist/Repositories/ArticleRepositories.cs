﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;


namespace MyDentist.Repositories
{
    public class ArticleRepositories
    {
        private readonly db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, ArticleViewModel contentViewModel)
        {
            contentViewModel.Picture = ConvertToBytes(file);
            var Content = new tbl_Article
            {
                Tittle = contentViewModel.Tittle,
                Writer_name = contentViewModel.Writer_name,
                Content = contentViewModel.Content,
                Release_date = DateTime.Today,
                Picture = contentViewModel.Picture
            };

            db.tbl_Article.Add(Content);
            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}
