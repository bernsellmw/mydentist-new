﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MyDentist.Repositories
{
    public class DentistRepositories
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_Dentist contentViewModel, int id)
        {

            contentViewModel.Picture = ConvertToBytes(file);
            var d = db.tbl_Dentist.Where(a => a.ID_Dentist == id).FirstOrDefault();
            d.Name = contentViewModel.Name;

            d.Phone = contentViewModel.Phone;
            d.Address = contentViewModel.Address;
            d.Picture = contentViewModel.Picture;
            d.Birth_date = contentViewModel.Birth_date;
            db.Configuration.ValidateOnSaveEnabled = false;

            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}