﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
namespace MyDentist.Repositories
{
    public class ConfirmPaymentRepositories
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_payment contentViewModel, int id)
        {

            using (DbContextTransaction dbtran = db.Database.BeginTransaction())
            {
                try
                {
                    contentViewModel.Picture = ConvertToBytes(file);
                    var p = db.tbl_payment.Where(a => a.ID_Payment == id).FirstOrDefault();
                    p.IsPaid = true;
                    p.Picture = contentViewModel.Picture;
                    p.Account_number = contentViewModel.Account_number;
                    p.Account_owner = contentViewModel.Account_owner;
                    p.bank_name = contentViewModel.bank_name;
                    db.Configuration.ValidateOnSaveEnabled = false;

                    int i = db.SaveChanges();
                    dbtran.Commit();
                    if (i == 1)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (DbEntityValidationException err)
                {
                    dbtran.Rollback();
                    throw;
                }
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}