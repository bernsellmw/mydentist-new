﻿using MyDentist.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;

namespace MyDentist.Repositories
{
    public class EditClinicRepositories
    {
        private db_mydentistEntities1 db = new db_mydentistEntities1();
        public int UploadImageInDataBase(HttpPostedFileBase file, tbl_Clinic contentViewModel, int id)
        {

            using (DbContextTransaction dbtran = db.Database.BeginTransaction())
            {
                try
                {
                    contentViewModel.Picture = ConvertToBytes(file);
                    var c = db.tbl_Clinic.Where(a => a.ID_Clinic == id).FirstOrDefault();
                    c.Clinic_Name = contentViewModel.Clinic_Name;
                    c.Address = contentViewModel.Address;
                    c.Phone = contentViewModel.Phone;
                    c.Open_hour = contentViewModel.Open_hour;
                    c.Close_hour = contentViewModel.Close_hour;
                    c.Patient_limit = contentViewModel.Patient_limit;
                    c.Picture = contentViewModel.Picture;
                    db.Configuration.ValidateOnSaveEnabled = false;

                    int i = db.SaveChanges();
                    if (i == 1)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (DbEntityValidationException err)
                {

                    throw;
                    return 0;
                }
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}