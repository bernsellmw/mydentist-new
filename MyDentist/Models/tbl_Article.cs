//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyDentist.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Article
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_Article()
        {
            this.tbl_Read = new HashSet<tbl_Read>();
        }
    
        public int ID_Article { get; set; }
        public string Tittle { get; set; }
        public string Content { get; set; }
        public string Writer_name { get; set; }
        public Nullable<System.DateTime> Release_date { get; set; }
        public byte[] Picture { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Read> tbl_Read { get; set; }
    }
}
