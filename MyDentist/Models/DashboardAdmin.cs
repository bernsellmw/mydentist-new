﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace MyDentist.Models
{
    public class DashboardAdmin
    {
        public int PatientUser { get; set; }
        public int DentistUser { get; set; }
        public int ReservationMade { get; set; }
        public int Clinics { get; set; }
        public int ReservationsToday { get; set; }

    }
}