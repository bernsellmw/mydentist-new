
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/03/2021 06:49:10
-- Generated from EDMX file: D:\BootCamp Nawadata\Level 2\Task\MyDentist 2\MyDentist\Models\MyDentist.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [db_mydentist];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[fk_feedback_clinic]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Feedback] DROP CONSTRAINT [fk_feedback_clinic];
GO
IF OBJECT_ID(N'[dbo].[fk_payment_reservation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_payment] DROP CONSTRAINT [fk_payment_reservation];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_Article]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Read] DROP CONSTRAINT [fk_tbl_Article];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_Clinic]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Schedule] DROP CONSTRAINT [fk_tbl_Clinic];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_clinic_services]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Services] DROP CONSTRAINT [fk_tbl_clinic_services];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_Dentist]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Schedule] DROP CONSTRAINT [fk_tbl_Dentist];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_Patient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Reservation] DROP CONSTRAINT [fk_tbl_Patient];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_Reservation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Feedback] DROP CONSTRAINT [fk_tbl_Reservation];
GO
IF OBJECT_ID(N'[dbo].[fk_tbl_Schedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Reservation] DROP CONSTRAINT [fk_tbl_Schedule];
GO
IF OBJECT_ID(N'[dbo].[fk_transaction_reservation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Transaction] DROP CONSTRAINT [fk_transaction_reservation];
GO
IF OBJECT_ID(N'[dbo].[fk_transaction_services]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_Transaction] DROP CONSTRAINT [fk_transaction_services];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[tbl_Admin]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Admin];
GO
IF OBJECT_ID(N'[dbo].[tbl_Article]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Article];
GO
IF OBJECT_ID(N'[dbo].[tbl_Audit_Trail]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Audit_Trail];
GO
IF OBJECT_ID(N'[dbo].[tbl_bank]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_bank];
GO
IF OBJECT_ID(N'[dbo].[tbl_Clinic]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Clinic];
GO
IF OBJECT_ID(N'[dbo].[tbl_Dentist]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Dentist];
GO
IF OBJECT_ID(N'[dbo].[tbl_error_log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_error_log];
GO
IF OBJECT_ID(N'[dbo].[tbl_Feedback]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Feedback];
GO
IF OBJECT_ID(N'[dbo].[tbl_Patient]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Patient];
GO
IF OBJECT_ID(N'[dbo].[tbl_payment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_payment];
GO
IF OBJECT_ID(N'[dbo].[tbl_Read]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Read];
GO
IF OBJECT_ID(N'[dbo].[tbl_Reservation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Reservation];
GO
IF OBJECT_ID(N'[dbo].[tbl_Schedule]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Schedule];
GO
IF OBJECT_ID(N'[dbo].[tbl_Services]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Services];
GO
IF OBJECT_ID(N'[dbo].[tbl_Transaction]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_Transaction];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'tbl_Article'
CREATE TABLE [dbo].[tbl_Article] (
    [ID_Article] int IDENTITY(1,1) NOT NULL,
    [Tittle] varchar(100)  NULL,
    [Content] varchar(max)  NULL,
    [Writer_name] varchar(100)  NULL,
    [Release_date] datetime  NULL,
    [Picture] varbinary(max)  NULL
);
GO

-- Creating table 'tbl_Clinic'
CREATE TABLE [dbo].[tbl_Clinic] (
    [ID_Clinic] int IDENTITY(1,1) NOT NULL,
    [Address] nvarchar(255)  NULL,
    [Phone] nvarchar(255)  NULL,
    [Open_hour] time  NULL,
    [Close_hour] time  NULL,
    [Patient_limit] int  NULL,
    [Picture] varbinary(max)  NULL,
    [Clinic_Name] nvarchar(255)  NULL,
    [Current_Queue] int  NULL,
    [Latitude] decimal(8,6)  NULL,
    [Longitude] decimal(9,6)  NULL
);
GO

-- Creating table 'tbl_Dentist'
CREATE TABLE [dbo].[tbl_Dentist] (
    [ID_Dentist] int IDENTITY(1,1) NOT NULL,
    [Username] varchar(50)  NULL,
    [Password] nvarchar(max)  NULL,
    [Name] varchar(255)  NULL,
    [Age] int  NULL,
    [Birth_date] datetime  NULL,
    [Address] varchar(255)  NULL,
    [Email] varchar(50)  NULL,
    [NPA] varchar(50)  NULL,
    [Picture] varbinary(max)  NULL,
    [IsEmailVerified] bit  NULL,
    [ActivationCode] uniqueidentifier  NULL,
    [ResetPassword] nvarchar(100)  NULL,
    [Phone] varchar(15)  NULL
);
GO

-- Creating table 'tbl_Feedback'
CREATE TABLE [dbo].[tbl_Feedback] (
    [ID_Feedback] int IDENTITY(1,1) NOT NULL,
    [Score] int  NULL,
    [FB_Date] datetime  NULL,
    [FB_Time] time  NULL,
    [ID_Reservation] int  NULL,
    [Comment] nvarchar(100)  NULL,
    [ID_Clinic] int  NULL
);
GO

-- Creating table 'tbl_Patient'
CREATE TABLE [dbo].[tbl_Patient] (
    [ID_Patient] int IDENTITY(1,1) NOT NULL,
    [Username] varchar(20)  NULL,
    [Password] nvarchar(max)  NULL,
    [Name] varchar(100)  NULL,
    [Age] int  NULL,
    [Birth_date] datetime  NULL,
    [Email] varchar(50)  NULL,
    [Phone] varchar(15)  NULL,
    [Address] varchar(255)  NULL,
    [Picture] varbinary(max)  NULL,
    [IsEmailVerified] bit  NULL,
    [ActivationCode] uniqueidentifier  NULL,
    [ResetPassword] nvarchar(100)  NULL,
    [RecentLatitude] decimal(8,6)  NULL,
    [RecentLongitude] decimal(9,6)  NULL
);
GO

-- Creating table 'tbl_Read'
CREATE TABLE [dbo].[tbl_Read] (
    [ID_Read] int IDENTITY(1,1) NOT NULL,
    [Reader] varchar(100)  NULL,
    [Date_read] datetime  NULL,
    [ID_Article] int  NULL
);
GO

-- Creating table 'tbl_Reservation'
CREATE TABLE [dbo].[tbl_Reservation] (
    [ID_Reservation] int IDENTITY(1,1) NOT NULL,
    [Reservation_Date] datetime  NULL,
    [Reservation_status] nvarchar(30)  NULL,
    [Queue_number] int  NULL,
    [ID_Patient] int  NULL,
    [ID_Schedule] int  NULL,
    [Clinic_ID] int  NULL,
    [Clinic_Name] varchar(255)  NULL,
    [Patient_Name] varchar(100)  NULL,
    [Price] int  NULL,
    [note] varchar(100)  NULL
);
GO

-- Creating table 'tbl_Schedule'
CREATE TABLE [dbo].[tbl_Schedule] (
    [ID_Schedule] int IDENTITY(1,1) NOT NULL,
    [Start_hour] time  NULL,
    [End_hour] time  NULL,
    [ID_Dentist] int  NULL,
    [ID_Clinic] int  NULL
);
GO

-- Creating table 'tbl_Admin'
CREATE TABLE [dbo].[tbl_Admin] (
    [ID_Admin] int IDENTITY(1,1) NOT NULL,
    [Username] varchar(20)  NULL,
    [Password] nvarchar(max)  NULL,
    [Name] varchar(100)  NULL,
    [Age] int  NULL,
    [Birth_date] datetime  NULL,
    [Email] varchar(50)  NULL,
    [Phone] varchar(15)  NULL,
    [Address] varchar(255)  NULL,
    [IsEmailVerified] bit  NULL,
    [ActivationCode] uniqueidentifier  NULL,
    [Picture] varbinary(max)  NULL
);
GO

-- Creating table 'tbl_Services'
CREATE TABLE [dbo].[tbl_Services] (
    [ID_services] int IDENTITY(1,1) NOT NULL,
    [Services_Name] varchar(100)  NULL,
    [Services_Price] int  NULL,
    [ID_Clinic] int  NULL
);
GO

-- Creating table 'tbl_Audit_Trail'
CREATE TABLE [dbo].[tbl_Audit_Trail] (
    [ID_audittrail] int IDENTITY(1,1) NOT NULL,
    [tbl_name] varchar(20)  NULL,
    [column_name] nvarchar(max)  NULL,
    [action_date] datetime  NULL,
    [action_hour] time  NULL,
    [action_type] varchar(20)  NULL,
    [old_value] nvarchar(max)  NULL,
    [new_value] nvarchar(max)  NULL
);
GO

-- Creating table 'tbl_error_log'
CREATE TABLE [dbo].[tbl_error_log] (
    [ID_Error] int IDENTITY(1,1) NOT NULL,
    [error_type] nvarchar(max)  NULL,
    [error_message] nvarchar(max)  NULL,
    [table_name] varchar(20)  NULL,
    [column_name] varchar(20)  NULL
);
GO

-- Creating table 'tbl_Transaction'
CREATE TABLE [dbo].[tbl_Transaction] (
    [ID_transaction] int IDENTITY(1,1) NOT NULL,
    [ID_Services] int  NULL,
    [Services_Name] varchar(100)  NULL,
    [Services_Price] int  NULL,
    [ID_Reservation] int  NULL,
    [Transaction_date] datetime  NULL,
    [quantity] int  NULL
);
GO

-- Creating table 'tbl_payment'
CREATE TABLE [dbo].[tbl_payment] (
    [ID_Payment] int IDENTITY(1,1) NOT NULL,
    [ID_Reservation] int  NULL,
    [Picture] varbinary(max)  NULL,
    [IsPaid] bit  NULL,
    [price] int  NULL,
    [Account_number] nvarchar(30)  NULL,
    [Account_owner] nvarchar(100)  NULL,
    [bank_name] nvarchar(30)  NULL
);
GO

-- Creating table 'tbl_bank'
CREATE TABLE [dbo].[tbl_bank] (
    [ID_bank] int IDENTITY(1,1) NOT NULL,
    [bank_name] nvarchar(30)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID_Article] in table 'tbl_Article'
ALTER TABLE [dbo].[tbl_Article]
ADD CONSTRAINT [PK_tbl_Article]
    PRIMARY KEY CLUSTERED ([ID_Article] ASC);
GO

-- Creating primary key on [ID_Clinic] in table 'tbl_Clinic'
ALTER TABLE [dbo].[tbl_Clinic]
ADD CONSTRAINT [PK_tbl_Clinic]
    PRIMARY KEY CLUSTERED ([ID_Clinic] ASC);
GO

-- Creating primary key on [ID_Dentist] in table 'tbl_Dentist'
ALTER TABLE [dbo].[tbl_Dentist]
ADD CONSTRAINT [PK_tbl_Dentist]
    PRIMARY KEY CLUSTERED ([ID_Dentist] ASC);
GO

-- Creating primary key on [ID_Feedback] in table 'tbl_Feedback'
ALTER TABLE [dbo].[tbl_Feedback]
ADD CONSTRAINT [PK_tbl_Feedback]
    PRIMARY KEY CLUSTERED ([ID_Feedback] ASC);
GO

-- Creating primary key on [ID_Patient] in table 'tbl_Patient'
ALTER TABLE [dbo].[tbl_Patient]
ADD CONSTRAINT [PK_tbl_Patient]
    PRIMARY KEY CLUSTERED ([ID_Patient] ASC);
GO

-- Creating primary key on [ID_Read] in table 'tbl_Read'
ALTER TABLE [dbo].[tbl_Read]
ADD CONSTRAINT [PK_tbl_Read]
    PRIMARY KEY CLUSTERED ([ID_Read] ASC);
GO

-- Creating primary key on [ID_Reservation] in table 'tbl_Reservation'
ALTER TABLE [dbo].[tbl_Reservation]
ADD CONSTRAINT [PK_tbl_Reservation]
    PRIMARY KEY CLUSTERED ([ID_Reservation] ASC);
GO

-- Creating primary key on [ID_Schedule] in table 'tbl_Schedule'
ALTER TABLE [dbo].[tbl_Schedule]
ADD CONSTRAINT [PK_tbl_Schedule]
    PRIMARY KEY CLUSTERED ([ID_Schedule] ASC);
GO

-- Creating primary key on [ID_Admin] in table 'tbl_Admin'
ALTER TABLE [dbo].[tbl_Admin]
ADD CONSTRAINT [PK_tbl_Admin]
    PRIMARY KEY CLUSTERED ([ID_Admin] ASC);
GO

-- Creating primary key on [ID_services] in table 'tbl_Services'
ALTER TABLE [dbo].[tbl_Services]
ADD CONSTRAINT [PK_tbl_Services]
    PRIMARY KEY CLUSTERED ([ID_services] ASC);
GO

-- Creating primary key on [ID_audittrail] in table 'tbl_Audit_Trail'
ALTER TABLE [dbo].[tbl_Audit_Trail]
ADD CONSTRAINT [PK_tbl_Audit_Trail]
    PRIMARY KEY CLUSTERED ([ID_audittrail] ASC);
GO

-- Creating primary key on [ID_Error] in table 'tbl_error_log'
ALTER TABLE [dbo].[tbl_error_log]
ADD CONSTRAINT [PK_tbl_error_log]
    PRIMARY KEY CLUSTERED ([ID_Error] ASC);
GO

-- Creating primary key on [ID_transaction] in table 'tbl_Transaction'
ALTER TABLE [dbo].[tbl_Transaction]
ADD CONSTRAINT [PK_tbl_Transaction]
    PRIMARY KEY CLUSTERED ([ID_transaction] ASC);
GO

-- Creating primary key on [ID_Payment] in table 'tbl_payment'
ALTER TABLE [dbo].[tbl_payment]
ADD CONSTRAINT [PK_tbl_payment]
    PRIMARY KEY CLUSTERED ([ID_Payment] ASC);
GO

-- Creating primary key on [ID_bank] in table 'tbl_bank'
ALTER TABLE [dbo].[tbl_bank]
ADD CONSTRAINT [PK_tbl_bank]
    PRIMARY KEY CLUSTERED ([ID_bank] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ID_Article] in table 'tbl_Read'
ALTER TABLE [dbo].[tbl_Read]
ADD CONSTRAINT [fk_tbl_Article_Read]
    FOREIGN KEY ([ID_Article])
    REFERENCES [dbo].[tbl_Article]
        ([ID_Article])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_Article_Read'
CREATE INDEX [IX_fk_tbl_Article_Read]
ON [dbo].[tbl_Read]
    ([ID_Article]);
GO

-- Creating foreign key on [ID_Clinic] in table 'tbl_Schedule'
ALTER TABLE [dbo].[tbl_Schedule]
ADD CONSTRAINT [fk_tbl_Clinic]
    FOREIGN KEY ([ID_Clinic])
    REFERENCES [dbo].[tbl_Clinic]
        ([ID_Clinic])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_Clinic'
CREATE INDEX [IX_fk_tbl_Clinic]
ON [dbo].[tbl_Schedule]
    ([ID_Clinic]);
GO

-- Creating foreign key on [ID_Dentist] in table 'tbl_Schedule'
ALTER TABLE [dbo].[tbl_Schedule]
ADD CONSTRAINT [fk_tbl_Dentist]
    FOREIGN KEY ([ID_Dentist])
    REFERENCES [dbo].[tbl_Dentist]
        ([ID_Dentist])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_Dentist'
CREATE INDEX [IX_fk_tbl_Dentist]
ON [dbo].[tbl_Schedule]
    ([ID_Dentist]);
GO

-- Creating foreign key on [ID_Reservation] in table 'tbl_Feedback'
ALTER TABLE [dbo].[tbl_Feedback]
ADD CONSTRAINT [fk_tbl_Reservation_Feedback]
    FOREIGN KEY ([ID_Reservation])
    REFERENCES [dbo].[tbl_Reservation]
        ([ID_Reservation])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_Reservation_Feedback'
CREATE INDEX [IX_fk_tbl_Reservation_Feedback]
ON [dbo].[tbl_Feedback]
    ([ID_Reservation]);
GO

-- Creating foreign key on [ID_Patient] in table 'tbl_Reservation'
ALTER TABLE [dbo].[tbl_Reservation]
ADD CONSTRAINT [fk_tbl_Dentist_Reservation]
    FOREIGN KEY ([ID_Patient])
    REFERENCES [dbo].[tbl_Patient]
        ([ID_Patient])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_Dentist_Reservation'
CREATE INDEX [IX_fk_tbl_Dentist_Reservation]
ON [dbo].[tbl_Reservation]
    ([ID_Patient]);
GO

-- Creating foreign key on [ID_Schedule] in table 'tbl_Reservation'
ALTER TABLE [dbo].[tbl_Reservation]
ADD CONSTRAINT [fk_tbl_Schedule_Reservation]
    FOREIGN KEY ([ID_Schedule])
    REFERENCES [dbo].[tbl_Schedule]
        ([ID_Schedule])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_Schedule_Reservation'
CREATE INDEX [IX_fk_tbl_Schedule_Reservation]
ON [dbo].[tbl_Reservation]
    ([ID_Schedule]);
GO

-- Creating foreign key on [ID_Clinic] in table 'tbl_Services'
ALTER TABLE [dbo].[tbl_Services]
ADD CONSTRAINT [fk_tbl_clinic_services]
    FOREIGN KEY ([ID_Clinic])
    REFERENCES [dbo].[tbl_Clinic]
        ([ID_Clinic])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_tbl_clinic_services'
CREATE INDEX [IX_fk_tbl_clinic_services]
ON [dbo].[tbl_Services]
    ([ID_Clinic]);
GO

-- Creating foreign key on [ID_Reservation] in table 'tbl_Transaction'
ALTER TABLE [dbo].[tbl_Transaction]
ADD CONSTRAINT [fk_transaction_reservation]
    FOREIGN KEY ([ID_Reservation])
    REFERENCES [dbo].[tbl_Reservation]
        ([ID_Reservation])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_transaction_reservation'
CREATE INDEX [IX_fk_transaction_reservation]
ON [dbo].[tbl_Transaction]
    ([ID_Reservation]);
GO

-- Creating foreign key on [ID_Services] in table 'tbl_Transaction'
ALTER TABLE [dbo].[tbl_Transaction]
ADD CONSTRAINT [fk_transaction_services]
    FOREIGN KEY ([ID_Services])
    REFERENCES [dbo].[tbl_Services]
        ([ID_services])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_transaction_services'
CREATE INDEX [IX_fk_transaction_services]
ON [dbo].[tbl_Transaction]
    ([ID_Services]);
GO

-- Creating foreign key on [ID_Reservation] in table 'tbl_payment'
ALTER TABLE [dbo].[tbl_payment]
ADD CONSTRAINT [fk_payment_reservation]
    FOREIGN KEY ([ID_Reservation])
    REFERENCES [dbo].[tbl_Reservation]
        ([ID_Reservation])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_payment_reservation'
CREATE INDEX [IX_fk_payment_reservation]
ON [dbo].[tbl_payment]
    ([ID_Reservation]);
GO

-- Creating foreign key on [ID_Clinic] in table 'tbl_Feedback'
ALTER TABLE [dbo].[tbl_Feedback]
ADD CONSTRAINT [fk_feedback_clinic]
    FOREIGN KEY ([ID_Clinic])
    REFERENCES [dbo].[tbl_Clinic]
        ([ID_Clinic])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_feedback_clinic'
CREATE INDEX [IX_fk_feedback_clinic]
ON [dbo].[tbl_Feedback]
    ([ID_Clinic]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------