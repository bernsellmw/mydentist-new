﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDentist.Models
{
    public class DashboardDentistcs
    {
        public int PatientToday { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "Rp {0:#,0}", ApplyFormatInEditMode = true)]
        public int? income { get; set; }
        public string ClinicScore { get; set; }



    }
}