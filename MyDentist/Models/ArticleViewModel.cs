﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyDentist.Models
{
    public class ArticleViewModel
    {
        [Display(Name = "ID Article")]
        public int ID_Article { get; set; }
        [Required]

        [Display(Name = "Title")]
        public string Tittle { get; set; }

        [Display(Name = "Writer Name")]
        [Required]
        public string Writer_name { get; set; }

        [Required]
        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> Release_date { get; set; }

        [Display(Name = "Content")]
        [AllowHtml]
        [Required]
        public string Content { get; set; }

        [Display(Name = "Picture")]
        [Required]
        public byte[] Picture { get; set; }
    }
}