﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDentist.Models
{
    public class GetLocationPatient
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}