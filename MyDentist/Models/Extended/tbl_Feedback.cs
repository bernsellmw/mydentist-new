﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDentist.Models
{
    [MetadataType(typeof(tbl_FeedbackMetadata))]
    public partial class tbl_Feedback
    {
    }

    public class tbl_FeedbackMetadata
    {
        [Required(ErrorMessage ="Score can not be empty ")]
        public int Score { get; set; }
        
        public System.DateTime FB_Date { get; set; }
        public int ID_Reservation { get; set; }
        public string Comment { get; set; }
    }
}