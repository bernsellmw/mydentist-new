﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MyDentist.Models
{
    [MetadataType(typeof(tbl_ClinicMetadata))]
    public partial class tbl_Clinic
    {
    }
    public class tbl_ClinicMetadata
    {
        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address required")]
        public string Address { get; set; }
        [Display(Name = "Phone Number")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "Only Number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone Number required")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Display(Name = "Open Hour")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Open Hour required")]
        [DataType(DataType.Time)]
        
        public System.TimeSpan Open_hour { get; set; }
        [Display(Name = "Close Hour")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Close Hour required")]
        [DataType(DataType.Time)]
       
        public System.TimeSpan Close_hour { get; set; }
        [Display(Name = "Patient Limit")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Patient Limit required")]
        public int Patient_limit { get; set; }
        [Display(Name = "Clinic Picture")]
        
        public byte[] Picture { get; set; }
        [Display(Name = "Clinic Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Clinic Name required")]
        public string Clinic_Name { get; set; }
    }
}