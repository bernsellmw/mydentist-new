﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyDentist.Models
{
    [MetadataType(typeof(tbl_paymentMetadata))]
    public partial class tbl_payment
    {
        public List<tbl_bank> BankList { get; internal set; }
    }
    public class tbl_paymentMetadata
    {
        public Nullable<bool> IsPaid { get; set; }
        public Nullable<int> price { get; set; }
        public string Account_number { get; set; }
        public string Account_owner { get; set; }
        public string bank_name { get; set; }
        public int ID_Payment { get; set; }
        public Nullable<int> ID_Reservation { get; set; }
        [Required]
        public byte[] Picture { get; set; }
        [NotMapped]
        public List<tbl_bank>BankList { get; set; }
        
        
    }
}