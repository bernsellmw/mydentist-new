﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDentist.Models
{
    [MetadataType(typeof(tbl_ServicesMetadata))]
    public partial class tbl_Services
    {
        public Nullable<int> quantity { get; set; }
    }

    public class tbl_ServicesMetadata
    {
        [Display(Name = "Service Name ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Service Name required")]
        public string Services_Name { get; set; }
        [Display(Name = "Service Price ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Service Price required")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "Rp {0:#,0}", ApplyFormatInEditMode = true)]
        public int Services_Price { get; set; }

        [Display (Name="Quantity")]
        public int quantity { get; set; }
        //public int ID_Clinic { get; set; }
    }
}