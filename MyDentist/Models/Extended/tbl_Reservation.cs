﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MyDentist.Models
{
    [MetadataType(typeof(tbl_ReservationsMetadata))]
    public partial class tbl_Reservation
    {

    }
    public class tbl_ReservationsMetadata
    {
        [Display(Name = "Reservation Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public System.DateTime Reservation_Date { get; set; }

        [Display(Name = "Reservation Status")]
        public string Reservation_status { get; set; }

        [Display(Name = "Queue Number")]
        public int Queue_number { get; set; }

        [Display(Name = "ID Patient")]
        public int ID_Patient { get; set; }

        [Display(Name = "ID Schedule")]
        public int ID_Schedule { get; set; }

        [Display(Name = "Clinic ID")]
        public int Clinic_ID { get; set; }

        [Display(Name = "Clinic Name")]
        public string Clinic_Name { get; set; }

        [Display(Name = "Patient Name")]
        public string Patient_Name { get; set; }

        [Display(Name = "Service Price ")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "Rp {0:#,0}", ApplyFormatInEditMode = true)]
        public Nullable<int> Price { get; set; }

        public string note { get; set; }
    }
}