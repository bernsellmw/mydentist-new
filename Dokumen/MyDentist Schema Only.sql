USE [master]
GO
/****** Object:  Database [db_mydentist]    Script Date: 02/02/2021 20:27:37 ******/
CREATE DATABASE [db_mydentist]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_mydentist', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_mydentist.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_mydentist_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_mydentist_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [db_mydentist] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_mydentist].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_mydentist] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_mydentist] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_mydentist] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_mydentist] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_mydentist] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_mydentist] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_mydentist] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_mydentist] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_mydentist] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_mydentist] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_mydentist] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_mydentist] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_mydentist] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_mydentist] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_mydentist] SET  ENABLE_BROKER 
GO
ALTER DATABASE [db_mydentist] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_mydentist] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_mydentist] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_mydentist] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_mydentist] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_mydentist] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_mydentist] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_mydentist] SET RECOVERY FULL 
GO
ALTER DATABASE [db_mydentist] SET  MULTI_USER 
GO
ALTER DATABASE [db_mydentist] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_mydentist] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_mydentist] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_mydentist] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_mydentist] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_mydentist] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'db_mydentist', N'ON'
GO
ALTER DATABASE [db_mydentist] SET QUERY_STORE = OFF
GO
USE [db_mydentist]
GO
/****** Object:  Table [dbo].[OLE DB Destination]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OLE DB Destination](
	[Clinic_Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Open_hour] [datetime] NULL,
	[Close_hour] [datetime] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Patient_limit] [float] NULL,
	[Phone] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Admin]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Admin](
	[ID_Admin] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](20) NULL,
	[Password] [nvarchar](max) NULL,
	[Name] [varchar](100) NULL,
	[Age] [int] NULL,
	[Birth_date] [date] NULL,
	[Email] [varchar](50) NULL,
	[Phone] [varchar](15) NULL,
	[Address] [varchar](255) NULL,
	[IsEmailVerified] [bit] NULL,
	[ActivationCode] [uniqueidentifier] NULL,
	[Picture] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Admin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Article]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Article](
	[ID_Article] [int] IDENTITY(1,1) NOT NULL,
	[Tittle] [varchar](100) NULL,
	[Content] [varchar](max) NULL,
	[Writer_name] [varchar](100) NULL,
	[Release_date] [date] NULL,
	[Picture] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Article] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Audit_Trail]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Audit_Trail](
	[ID_audittrail] [int] IDENTITY(1,1) NOT NULL,
	[tbl_name] [varchar](20) NULL,
	[column_name] [nvarchar](max) NULL,
	[action_date] [date] NULL,
	[action_hour] [time](7) NULL,
	[action_type] [varchar](20) NULL,
	[old_value] [nvarchar](max) NULL,
	[new_value] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_audittrail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bank]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bank](
	[ID_bank] [int] IDENTITY(1,1) NOT NULL,
	[bank_name] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_bank] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Clinic]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Clinic](
	[ID_Clinic] [int] IDENTITY(1,1) NOT NULL,
	[Address] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[Open_hour] [time](0) NULL,
	[Close_hour] [time](0) NULL,
	[Patient_limit] [int] NULL,
	[Picture] [varbinary](max) NULL,
	[Clinic_Name] [nvarchar](255) NULL,
	[Current_Queue] [int] NULL,
	[Latitude] [decimal](8, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Clinic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Dentist]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Dentist](
	[ID_Dentist] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [nvarchar](max) NULL,
	[Name] [varchar](255) NULL,
	[Age] [int] NULL,
	[Birth_date] [date] NULL,
	[Address] [varchar](255) NULL,
	[Email] [varchar](50) NULL,
	[NPA] [varchar](50) NULL,
	[Picture] [varbinary](max) NULL,
	[IsEmailVerified] [bit] NULL,
	[ActivationCode] [uniqueidentifier] NULL,
	[ResetPassword] [nvarchar](100) NULL,
	[Phone] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Dentist] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_error_log]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_error_log](
	[ID_Error] [int] IDENTITY(1,1) NOT NULL,
	[error_type] [nvarchar](max) NULL,
	[error_message] [nvarchar](max) NULL,
	[table_name] [varchar](20) NULL,
	[column_name] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Error] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Feedback]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Feedback](
	[ID_Feedback] [int] IDENTITY(1,1) NOT NULL,
	[Score] [int] NULL,
	[FB_Date] [date] NULL,
	[FB_Time] [time](0) NULL,
	[ID_Reservation] [int] NULL,
	[Comment] [nvarchar](100) NULL,
	[ID_Clinic] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Feedback] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Patient]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Patient](
	[ID_Patient] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](20) NULL,
	[Password] [nvarchar](max) NULL,
	[Name] [varchar](100) NULL,
	[Age] [int] NULL,
	[Birth_date] [date] NULL,
	[Email] [varchar](50) NULL,
	[Phone] [varchar](15) NULL,
	[Address] [varchar](255) NULL,
	[Picture] [varbinary](max) NULL,
	[IsEmailVerified] [bit] NULL,
	[ActivationCode] [uniqueidentifier] NULL,
	[ResetPassword] [nvarchar](100) NULL,
	[RecentLatitude] [decimal](8, 6) NULL,
	[RecentLongitude] [decimal](9, 6) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Patient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_payment]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_payment](
	[ID_Payment] [int] IDENTITY(1,1) NOT NULL,
	[ID_Reservation] [int] NULL,
	[Picture] [varbinary](max) NULL,
	[IsPaid] [bit] NULL,
	[price] [int] NULL,
	[Account_number] [nvarchar](30) NULL,
	[Account_owner] [nvarchar](100) NULL,
	[bank_name] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Payment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Read]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Read](
	[ID_Read] [int] IDENTITY(1,1) NOT NULL,
	[Reader] [varchar](100) NULL,
	[Date_read] [date] NULL,
	[ID_Article] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Read] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Reservation]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Reservation](
	[ID_Reservation] [int] IDENTITY(1,1) NOT NULL,
	[Reservation_Date] [date] NULL,
	[Reservation_status] [nvarchar](30) NULL,
	[Queue_number] [int] NULL,
	[ID_Patient] [int] NULL,
	[ID_Schedule] [int] NULL,
	[Clinic_ID] [int] NULL,
	[Clinic_Name] [varchar](255) NULL,
	[Patient_Name] [varchar](100) NULL,
	[Price] [int] NULL,
	[note] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Reservation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Schedule]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Schedule](
	[ID_Schedule] [int] IDENTITY(1,1) NOT NULL,
	[Start_hour] [time](0) NULL,
	[End_hour] [time](0) NULL,
	[ID_Dentist] [int] NULL,
	[ID_Clinic] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Schedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Services]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Services](
	[ID_services] [int] IDENTITY(1,1) NOT NULL,
	[Services_Name] [varchar](100) NULL,
	[Services_Price] [int] NULL,
	[ID_Clinic] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_services] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Transaction]    Script Date: 02/02/2021 20:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Transaction](
	[ID_transaction] [int] IDENTITY(1,1) NOT NULL,
	[ID_Services] [int] NULL,
	[Services_Name] [varchar](100) NULL,
	[Services_Price] [int] NULL,
	[ID_Reservation] [int] NULL,
	[Transaction_date] [date] NULL,
	[quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_transaction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Feedback]  WITH CHECK ADD  CONSTRAINT [fk_feedback_clinic] FOREIGN KEY([ID_Clinic])
REFERENCES [dbo].[tbl_Clinic] ([ID_Clinic])
GO
ALTER TABLE [dbo].[tbl_Feedback] CHECK CONSTRAINT [fk_feedback_clinic]
GO
ALTER TABLE [dbo].[tbl_Feedback]  WITH CHECK ADD  CONSTRAINT [fk_tbl_Reservation_Feedback] FOREIGN KEY([ID_Reservation])
REFERENCES [dbo].[tbl_Reservation] ([ID_Reservation])
GO
ALTER TABLE [dbo].[tbl_Feedback] CHECK CONSTRAINT [fk_tbl_Reservation_Feedback]
GO
ALTER TABLE [dbo].[tbl_payment]  WITH CHECK ADD  CONSTRAINT [fk_payment_reservation] FOREIGN KEY([ID_Reservation])
REFERENCES [dbo].[tbl_Reservation] ([ID_Reservation])
GO
ALTER TABLE [dbo].[tbl_payment] CHECK CONSTRAINT [fk_payment_reservation]
GO
ALTER TABLE [dbo].[tbl_Read]  WITH CHECK ADD  CONSTRAINT [fk_tbl_Article_Read] FOREIGN KEY([ID_Article])
REFERENCES [dbo].[tbl_Article] ([ID_Article])
GO
ALTER TABLE [dbo].[tbl_Read] CHECK CONSTRAINT [fk_tbl_Article_Read]
GO
ALTER TABLE [dbo].[tbl_Reservation]  WITH CHECK ADD  CONSTRAINT [fk_tbl_Dentist_Reservation] FOREIGN KEY([ID_Patient])
REFERENCES [dbo].[tbl_Patient] ([ID_Patient])
GO
ALTER TABLE [dbo].[tbl_Reservation] CHECK CONSTRAINT [fk_tbl_Dentist_Reservation]
GO
ALTER TABLE [dbo].[tbl_Reservation]  WITH CHECK ADD  CONSTRAINT [fk_tbl_Schedule_Reservation] FOREIGN KEY([ID_Schedule])
REFERENCES [dbo].[tbl_Schedule] ([ID_Schedule])
GO
ALTER TABLE [dbo].[tbl_Reservation] CHECK CONSTRAINT [fk_tbl_Schedule_Reservation]
GO
ALTER TABLE [dbo].[tbl_Schedule]  WITH CHECK ADD  CONSTRAINT [fk_tbl_Clinic] FOREIGN KEY([ID_Clinic])
REFERENCES [dbo].[tbl_Clinic] ([ID_Clinic])
GO
ALTER TABLE [dbo].[tbl_Schedule] CHECK CONSTRAINT [fk_tbl_Clinic]
GO
ALTER TABLE [dbo].[tbl_Schedule]  WITH CHECK ADD  CONSTRAINT [fk_tbl_Dentist] FOREIGN KEY([ID_Dentist])
REFERENCES [dbo].[tbl_Dentist] ([ID_Dentist])
GO
ALTER TABLE [dbo].[tbl_Schedule] CHECK CONSTRAINT [fk_tbl_Dentist]
GO
ALTER TABLE [dbo].[tbl_Services]  WITH CHECK ADD  CONSTRAINT [fk_tbl_clinic_services] FOREIGN KEY([ID_Clinic])
REFERENCES [dbo].[tbl_Clinic] ([ID_Clinic])
GO
ALTER TABLE [dbo].[tbl_Services] CHECK CONSTRAINT [fk_tbl_clinic_services]
GO
ALTER TABLE [dbo].[tbl_Transaction]  WITH CHECK ADD  CONSTRAINT [fk_transaction_reservation] FOREIGN KEY([ID_Reservation])
REFERENCES [dbo].[tbl_Reservation] ([ID_Reservation])
GO
ALTER TABLE [dbo].[tbl_Transaction] CHECK CONSTRAINT [fk_transaction_reservation]
GO
ALTER TABLE [dbo].[tbl_Transaction]  WITH CHECK ADD  CONSTRAINT [fk_transaction_services] FOREIGN KEY([ID_Services])
REFERENCES [dbo].[tbl_Services] ([ID_services])
GO
ALTER TABLE [dbo].[tbl_Transaction] CHECK CONSTRAINT [fk_transaction_services]
GO
USE [master]
GO
ALTER DATABASE [db_mydentist] SET  READ_WRITE 
GO
