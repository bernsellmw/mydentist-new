create database db_mydentist;

use db_mydentist;

CREATE TABLE tbl_Patient (
	ID_Patient int identity primary key,
	Username varchar(20),
	Password varchar(20),
	Name varchar(100),
	Age int,
	Birth_date date,
	Email varchar(50),
	Phone varchar(15),
	Address varchar(255),
	Picture varbinary(max));

CREATE TABLE tbl_Clinic (
	ID_Clinic int identity primary key,
	Address varchar(255),
	Phone varchar(15),
	Open_hour time(0),
	Close_hour time(0),
	Patient_limit int,
	Picture varbinary(max));

CREATE TABLE tbl_Dentist (
	ID_Dentist int identity primary key,
	Username varchar(50),
	Password varchar(50),
	Name varchar(255),
	Age int,
	Birth_date date,
	Address varchar(255),
	Email varchar(50),
	NPA varchar(50),
	Picture varbinary(max));

CREATE TABLE tbl_Article (
	ID_Article int identity primary key,
	Tittle varchar(100),
	Content varchar(max),
	Writer_name varchar(100),
	Release_date date,
	Picture varbinary(max));

CREATE TABLE tbl_Schedule (
	ID_Schedule int identity primary key,
	Start_hour time(0),
	End_hour time(0),
	ID_Dentist int,
	ID_Clinic int,
	CONSTRAINT fk_tbl_Dentist FOREIGN KEY (ID_Dentist) REFERENCES tbl_Dentist(ID_Dentist),
	CONSTRAINT fk_tbl_Clinic FOREIGN KEY (ID_Clinic) REFERENCES tbl_Clinic(ID_Clinic));

CREATE TABLE tbl_Reservation (
	ID_Reservation int identity primary key,
	Reservation_Date date,
	Reservation_status varchar(10),
	Queue_number int,
	ID_Patient int,
	ID_Schedule int,
	CONSTRAINT fk_tbl_Dentist_Reservation FOREIGN KEY (ID_Patient) REFERENCES tbl_Patient(ID_Patient),
	CONSTRAINT fk_tbl_Schedule_Reservation FOREIGN KEY (ID_Schedule) REFERENCES tbl_Schedule(ID_Schedule));

CREATE TABLE tbl_Feedback (
	ID_Feedback int identity primary key,
	Score int,
	FB_Date date,
	FB_Time time(0),
	ID_Reservation int,
	CONSTRAINT fk_tbl_Reservation_Feedback FOREIGN KEY (ID_Reservation) REFERENCES tbl_Reservation(ID_Reservation));

CREATE TABLE tbl_Read (
	ID_Read int identity primary key,
	Reader varchar(100),
	Date_read date,
	ID_Article int,
	CONSTRAINT fk_tbl_Article_Read FOREIGN KEY (ID_Article) REFERENCES tbl_Article(ID_Article));

alter table tbl_patient
add IsEmailVerified bit, ActivationCode uniqueidentifier;

alter table tbl_patient
alter column Password nvarchar(max);

use db_mydentist 

select * from tbl_Clinic

delete from tbl_patient where Email = 'cryogenic00@gmail.com'

alter table tbl_patient
add RecentLatitude Decimal(8,6), RecentLongitude Decimal(9,6)

alter table tbl_Clinic
add Clinic_Name varchar (255)

insert into tbl_Clinic (Address,Phone,Open_hour,Close_hour, Patient_limit,Clinic_Name)
values ('Jl. Satria Raya H 240', '0243513068','18:00','21:00',10,'Drg. Florentina Retno P.H')
insert into tbl_Clinic (Address,Phone,Open_hour,Close_hour, Patient_limit,Clinic_Name)
values ('Jl. Melati Utara No. 16', '0821-3733-7377','15:00','21:00',10,'Drg semarang Cecilia & Ririn,SpOrt dentist ortodonti klinik dokter gigi semarang'),
('Jl. Indraprasta No.116','0851-0091-9191','17:00','20:00',10,'Im Dental-Dokter Gigi Spesialis Kawat Behel Gigi-Implan')

select * from tbl_Dentist

delete from tbl_Reservation
delete from tbl_Schedule

alter table tbl_Reservation
add Clinic_ID int, Clinic_Name varchar(255),Patient_Name varchar (100)
 

drop from tbl_Schedule
select * from tbl_Schedule
select * from tbl_Clinic
select * from tbl_Reservation
delete from tbl_Clinic where Address is null
delete from tbl_Schedule where  ID_Dentist = 3
delete from tbl_Reservation where Clinic_ID = 10 is null
delete from tbl_Reservation where I = 3

select * from tbl_Schedule
select * from tbl_Clinic
select * from tbl_Dentist

select * from tbl_Reservation where Clinic_ID = 10
select * from tbl_Patient
insert into tbl_Reservation (Queue_number,Patient_Name,Clinic_ID,Reservation_status)
values(4,'Jono',10,'Waiting'),(5,'Joni',10,'Waiting')
select * from tbl_Article
delete from tbl_Reservation where ID_Patient is null
insert into tbl_Reservation (Queue_number,Patient_Name,Clinic_ID)
values(9,'Jono',17),(10,'Joni',17)
delete from tbl_patient where Email = 'bernardinusansell@gmail.com'

update tbl_Clinic  
set  Latitude=-6.969522092050485, Longitude =  110.41309076787464
where ID_Clinic = 1

update tbl_Clinic
set Latitude=-6.981790223240845, Longitude = 110.4183089936389
where ID_Clinic = 2
, 
update tbl_Clinic
set Latitude=-6.96530484798396, Longitude = 110.41444661266956
where ID_Clinic = 17
select * from tbl_Clinic

delete from tbl_Article where ID_Article = 1

delete from tbl_Schedule where Start_hour is null

delete from tbl_Clinic where ID_Clinic is null


delete from tbl_Patient where Email = 'bernardinusansell@gmail.com'

CREATE TABLE tbl_Admin (
	ID_Admin int identity primary key,
	Username varchar(20),
	Password nvarchar(max),
	Name varchar(100),
	Age int,
	Birth_date date,
	Email varchar(50),
	Phone varchar(15),
	Address varchar(255),
	IsEmailVerified bit, 
	ActivationCode uniqueidentifier,
	Picture varbinary(max));

drop table error_log
create table tbl_Audit_Trail
(
	ID_audittrail int identity primary key,
	tbl_name varchar(20),
	column_name varchar(20),
	action_date date,
	action_hour time,
	action_type varchar(20),
	old_value nvarchar(max),
	new_value nvarchar(max)
);
select * from tbl_Patient
create table tbl_error_log
(
	ID_Error int identity primary key,
	error_type nvarchar(max),
	error_message nvarchar(max),
	table_name varchar(20),
	column_name varchar(20),
)

create table tbl_Services
(
	ID_services int identity primary key,
	Services_Name varchar (100),
	Services_Price int,
	ID_Clinic int,
	constraint fk_tbl_clinic_services foreign key (ID_Clinic) references tbl_Clinic(ID_Clinic)

)

create table tbl_Transaction
(
	ID_transaction int identity primary key,
	ID_Services int,
	Services_Name varchar (100),
	Services_Price int,
	ID_Reservation int,
	constraint fk_transaction_services foreign key (ID_services) references tbl_Services(ID_services),
	constraint fk_transaction_reservation foreign key (ID_Reservation) references tbl_Reservation(ID_Reservation)
)
select * from tbl_Transaction
alter table tbl_Transaction
add Transaction_date date
alter table tbl_Transaction
add quantity int
drop database ReportServer
drop database ReportServerTempDB


alter table tbl_Clinic
add Current_Queue int, Latitude Decimal(8,6), Longitude Decimal(9,6)

select* from tbl_Clinic
select * from tbl_Services
select * from tbl_Transaction where
alter table tbl_Reservation
add Price int

select d.Name,d.Phone,c.Clinic_Name, c.Address, c.Open_hour, c.Close_hour from tbl_Dentist as d
inner join tbl_Schedule as s on  d.ID_Dentist = s.ID_Dentist
inner join tbl_Clinic as c on c.ID_Clinic = s.ID_Clinic

select r.ID_Patient,r.Reservation_Date, r.Clinic_Name,r.Patient_Name, r.Reservation_status, c.Address, p.Phone from tbl_Reservation r
inner join tbl_Patient p on p.ID_Patient = r.ID_Patient
inner join tbl_Clinic c on c.ID_Clinic = r.Clinic_ID

select r.ID_Reservation,r.Reservation_Date,r.Patient_Name , r.Queue_number, t.Services_Name,t.Services_Price from tbl_Reservation r
inner join tbl_Transaction t on r.ID_Reservation = t.ID_Reservation
delete from tbl_Transaction where ID_Reservation = 1029
delete from tbl_Reservation where Reservation_Date is null
delete from tbl_Transaction where quantity is null
select r.ID_Reservation,t.Services_Name,t.Services_Price,t.quantity, t.Transaction_date from tbl_Transaction t
inner join tbl_Reservation r on r.ID_Reservation = t.ID_Reservation
where r.ID_Reservation=

select * from tbl_feedback 

create table tbl_payment
(
	ID_Payment int identity primary key,
	ID_Reservation int,
	Picture varbinary(max),
	IsPaid bit,
	constraint fk_payment_reservation foreign key (ID_Reservation) references tbl_Reservation(ID_Reservation)
)



select * from tbl_Clinic
delete  from tbl_payment
alter table tbl_Audit_Trail
alter column column_name nvarchar(max)

alter table tbl_Reservation
alter column Reservation_status nvarchar(30)

create table tbl_payment
(
	ID_Payment int identity primary key,
	ID_Reservation int,
	Picture varbinary(max),
	IsPaid bit,
	constraint fk_payment_reservation foreign key (ID_Reservation) references tbl_Reservation(ID_Reservation)
)

alter table tbl_payment
add price int

alter table tbl_Feedback
add Comment  nvarchar(100)

alter table tbl_Feedback
add ID_Clinic int
alter table tbl_feedback
add constraint fk_feedback_clinic
foreign key (ID_Clinic) References tbl_Clinic(ID_Clinic)

select * from tbl_payment

delete from tbl_payment
where ID_Payment=16

alter table tbl_payment
add bank_ID int ,Account_number nvarchar(30), Account_owner nvarchar(100)

alter table tbl_payment
drop column bank_ID

alter table tbl_payment
add bank_name nvarchar(30)

create table tbl_bank
(
	ID_bank int identity primary key,
	bank_name nvarchar(30)
)

insert into tbl_bank(bank_name)
values ('BCA'),('Mandiri'),('BNI')

select * from tbl_patient

alter table tbl_Reservation
add note varchar(100)

alter table tbl_Clinic
alter column Address nvarchar(255)

alter table tbl_Clinic
alter column Clinic_name nvarchar(255)
alter table tbl_Clinic
alter column Phone nvarchar(255)