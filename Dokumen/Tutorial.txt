MyDentist
File SSRS Ada di folder Dokumen
Panduan penggunaan :
1. Download file aplikasi dari direktori gitlab
2. Buka Aplikasi di Visual Studio
3. Buka folder Models di Solution Explorer Visual Studio
4. Temukan MyDentist.edmx, kemudian klik dua kali hingga model database muncul
5. Klik kanan pada window model MyDentist.edmx, kemudian pilih Generate Database From Model
6. Copy Semua kode pada window DDl, kemudian buka SQL Server Management Studio
7. Buat Query baru, kemudian Paste kode yang sudah anda salin dari Window DDL
8. Jalankan Query tersebut atau jalankan query db_mydentist schema only yang ada pada folder "Dokumen" di repositori Git Aplikasi
8. Buka query db_mydentist data only yang ada pada folder "Dokumen" di repositori Git Aplikasi
9. Jalankan Query db_mydentist data only
10. Jalankan Program dengan cara menekan tombol run "IIS Server (Google Chrome) di menu atas Visual Studio
11.a. Masukan Username dan Password sesuai dengan username dan password yang ada di file teks "Username and Password" di folder Dokumen jika ingin menggunakan username dan password yang sudah dibuat
11.b. Lakukan Registrasi dengan cara memilih register dan isi semua field dengan benar

User Pasien 
Mencari Klinik terdekat :
1. Pilih menu Nearby Clinic
2. Pilih tombol set location
3. tekan tombol show clinics

MElakukan Reservasi 
1. Pilih salah satu klinik yang ditampilkan, tekan detail
2. tekan tombol book dentist. Jika klinik tersebut sudah penuh, maka tombol ini tidak akan muncul sehingga anda perlu memilih klinik lain
3. Tekan menu Reservation untuk melihat Reservasi yang sudah anda buat sebelumnya.
4. Tekan Cancel pada reservasi yang anda inginkan untuk dibatalkan

User Dentist
Memasukkan klinik baru
1. Tekan menu My Clinic
2. Tekan menu insert clinic
3. Masukan data dengan benar
4. Tekan submit
5. Tekan add Service untuk menambahkan layanan atau produk apa saja yang anda tawarkan dalam klinik anda

Memulai Pemeriksaan
1. Pergi ke menu My Schedule
2. Pilih Jadwal yang sesuai dengan tanggal praktek anda saat itu
3. tekan Start Examination
4. Tekan add service untuk menambahkan layanan yang sudah anda lakukan pada saat praktek
5. Jika sudah selesai, tekan checkout
6. Tekan tombol Next Pastient untuk memanggil dan memulai pemeriksaan pada pasien selanjutnya.

User Admin
1. Login melalui url /UserAdmin/Login
2. Masukkan username dan password admin yang ada di dalam file teks Username and Password 
3. Untuk mengakses Audit Trail tekan menu audit trail di navbar.
4. Untuk mengakses daftar Dentist User tekan menu User List pada navbar, kemudian tekan tombol Dentist
5. Untuk mengakses daftar Patient User tekan menu User List pada navbar, kemudian tekan tombol Patient
6. Untuk mengakses daftar klinik yang sudah terdaftar tekan menu Clinic List di navbar
